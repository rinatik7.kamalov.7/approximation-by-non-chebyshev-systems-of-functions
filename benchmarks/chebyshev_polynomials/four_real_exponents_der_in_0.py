import math as m
from pathlib import Path
from algorithms.one_dimensional_case.chebyshev_polynomials.chebyshev_polynomials_arbitrary_linear_functional import ChebyshevPolynomials


def f_1(x):
    return m.exp(- x)


def f_2(x):
    return m.exp(- 0.5 * x)


def f_3(x):
    return m.exp(- 0.1 * x)


def f_4(x):
    return m.exp(- 0.4 * x)


modif_remez_alg_test_one = ChebyshevPolynomials([f_1, f_2, f_3, f_4],
                                                [1, 1 / 2 ** 4, 1 / 10 ** 4, (2 / 5) ** 4],
                                                Path(__file__).stem,
                                                const_lin_bound=1, left=1, right=10)
modif_remez_alg_test_one.main_body()
