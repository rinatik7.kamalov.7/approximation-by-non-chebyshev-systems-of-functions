import math as m
from pathlib import Path

from algorithms.multidimensional_case.approximation_by_polynomials.modified_Remez_degeneracy_minimization \
    import ModifiedRemezAlgorithmFightWithDegeneracyMinimization


def f_0(x):
    return m.sqrt(abs((x[0] - 0.5) * (x[1] - 0.5))) - 2 * m.sqrt(abs((x[2] - 0.25))) \
           + 2 ** abs(x[2] - 0.75)
# abs((x[0] - 0.5) * (x[1] - 0.5)) + 3 * abs((x[0] - 0.25)) - 2 * abs(x[1] - 0.75)

# m.sin(100 * x[0] - 20 * x[1])


def f_1(x):
    return m.exp(- x[0] + 2 * x[1] + 3 * x[2])


def f_2(x):
    return m.sin(x[0] + x[1] + x[2])


def f_3(x):
    return m.cos(3 * x[0] - x[1] + 5 * x[2])


# Evaluation of rate of convergence
modif_remez_alg_test_one = ModifiedRemezAlgorithmFightWithDegeneracyMinimization(f_0,
                                                                                 [f_1, f_2, f_3],
                                                                                 3,
                                                                                 Path(__file__).parent.parts[-1],
                                                                                 Path(__file__).stem, right=1)
upper_minus_lower = modif_remez_alg_test_one.main_body()

