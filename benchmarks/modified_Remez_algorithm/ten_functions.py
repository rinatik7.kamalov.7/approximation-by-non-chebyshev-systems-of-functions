import math as m
from pathlib import Path

from algorithms.one_dimensional_case.approximation_by_polynomials.modified_Remez_algorithm import ModifiedRemezAlgorithm
from postprocessing.graphs.approximation.rate_of_convergence import rate_of_convergence


def f_0(x):
    return 3 * m.sin(x) - 7 * m.cos(x)


# 2 * m.sin(x) - 3 * m.cos(x)
def f_1(x):
    return m.exp(- 0.5 * x) * m.sin(- 0.4 * x)


def f_2(x):
    return m.exp(- 0.5 * x) * m.cos(- 0.4 * x)


def f_3(x):
    return m.exp(- 0.1 * x) * m.sin(0.2 * x)


def f_4(x):
    return m.exp(- 0.1 * x) * m.cos(0.2 * x)


def f_5(x):
    return x * m.exp(- 0.1 * x) * m.sin(0.2 * x)


def f_6(x):
    return x * m.exp(- 0.1 * x) * m.cos(0.2 * x)


def f_7(x):
    return m.sin(- 0.3 * x)


def f_8(x):
    return m.cos(- 0.3 * x)


def f_9(x):
    return m.exp(- 0.2 * x)


def f_10(x):
    return x * m.exp(- 0.2 * x)


'''
# Modified_Remez_Algorithm
modif_remez_alg_test_one = ModifiedRemezAlgorithm(f_0, [f_1, f_2, f_3, f_4, f_5, f_6, f_7, f_8, f_9, f_10], Path(__file__).stem, right=10)
upper_minus_lower = modif_remez_alg_test_one.main_body()
print(f"Старая оцека {upper_minus_lower}")

# Least_Square_Method
least_square = LeastSquareMethod(f_0, [f_1, f_2, f_3, f_4, f_5, f_6, f_7, f_8, f_9, f_10], 10000, right=10)
norm = least_square.main_body()
print(f"Новая оценка {norm}")
'''

# Evaluation of rate of convergence
modif_remez_alg_test_one = ModifiedRemezAlgorithm(f_0, [f_1, f_2, f_3, f_4, f_5, f_6, f_7, f_8, f_9, f_10],
                                                  Path(__file__).parent.parts[-1],
                                                  Path(__file__).stem, right=10)
upper_minus_lower = modif_remez_alg_test_one.main_body()
rate_of_convergence(upper_minus_lower)
