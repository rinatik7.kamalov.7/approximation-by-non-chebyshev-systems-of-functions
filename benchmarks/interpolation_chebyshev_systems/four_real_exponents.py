import json
import math as m
import numpy as np
from pathlib import Path
from algorithms.one_dimensional_case.interpolation_by_functions.interpolation import InterpolationChebyshevSystem


def f_0(x):
    return m.exp(-0.2 * x) + m.exp(-0.4 * x)

# m.sin(x)
# x ** 2
# m.exp(- 0.7 * x)
# 1 / (1 + 25 * x ** 2)
# m.exp(- 0.4 * x)


def f_1(x):
    return m.exp(- x)


def f_2(x):
    return m.exp(- 0.5 * x)


def f_3(x):
    return m.exp(- 0.1 * x)


left = 1
right = 10
functions = [f_1, f_2, f_3]
number_of_points = len([f_1, f_2, f_3])

# Interpolation by the zeros of Chebyshev systems.
json_path = Path.cwd().parent.parent / "results" / "chebyshev_polynomials" / \
            "four_real_exponents_value_in_0" \
            / "four_real_exponents_value_in_0.json"
json_results = json_path.open("r", encoding="UTF-8")
roots_of_chebyshev_polynomial = json.load(json_results)['roots_of_the_best_polynom']
print(f"Grid {roots_of_chebyshev_polynomial}")
modif_remez_alg_test_one = InterpolationChebyshevSystem(f_0, functions,
                                                        roots_of_chebyshev_polynomial,
                                                        left=left, right=right)
error_chebyshev_grid = modif_remez_alg_test_one.main_body()
print(error_chebyshev_grid)

# Interpolation by the uniform grid.
uniform_grid = np.linspace(left, right, number_of_points)
print(f"Grid {uniform_grid}")
modif_remez_alg_test_one = InterpolationChebyshevSystem(f_0, functions,
                                                        uniform_grid,
                                                        left=left, right=right)
error_chebyshev_grid = modif_remez_alg_test_one.main_body()
print(error_chebyshev_grid)
