import math as m
import matplotlib.pyplot as plt
import numpy as np
from numpy.linalg import eig
from pathlib import Path

from algorithms.one_dimensional_case.cut_tail_point.cut_tail_point_without_degeneracy \
    import CutTailPoint

alpha = -0.3216
beta = m.sqrt(2)

eig_values_1, _ = eig([[alpha, -1], [2, alpha]])
print(f"Eigenvalues of A_1 {eig_values_1}.")
eig_values_2, _ = eig([[alpha, -2], [1, alpha]])
print(f"Eigenvalues of A_2 {eig_values_2}.")


def f_1(x):
    return m.exp(alpha * x) * m.sin(beta * x)


def f_2(x):
    return m.exp(alpha * x) * m.cos(beta * x)


modif_remez_alg_test_one = CutTailPoint([f_1, f_2], right=5)
cut_tail_point_first = modif_remez_alg_test_one.main_body()
print(f"Cut tail point: {cut_tail_point_first}")

# Let us draw trajectory of our 2D system and save it in particular directory.

Path(r"C:\Users\rinat\PycharmProjects\Algorithms_for_approximation_by_non_Chebyshev_systems"
     r"\results\cut_tail_point\%s" % f"{Path(__file__).stem}").mkdir(parents=True, exist_ok=True)

interval = np.arange(0, 100, 0.05)
x = np.array([m.e ** (alpha * t) * m.cos(beta * t) for t in interval])
y = np.array([m.e ** (alpha * t) * m.sin(beta * t) for t in interval])

fig = plt.figure()
ax = fig.add_axes([0, 0, 1, 1])
ax.set_title("Symmetrized trajectory of system $x^{'}=Ax,x(0)=(0,1)^T$")
ax.set_xlabel('$x_1$')
ax.set_ylabel('$x_2$')
ax.grid(color='b', ls='-.', lw=0.25)
ax.plot(x, y, 'r', label=r'the trajectory $x(t)$', linewidth=2)
ax.plot(-x, -y, 'b', label=r'the symmetric trajectory $-x(t)$', linewidth=2)

'''
interval_until_cut_tail_point = np.arange(0, cut_tail_point, 0.05)
x_until_cut_tail = np.array([m.e ** (-0.1 * t) * m.sin(0.3 * t) for t in interval_until_cut_tail_point])
y_until_cut_tail = np.array([m.e ** (-0.1 * t) * m.cos(0.3 * t) for t in interval_until_cut_tail_point])
z_until_cut_tail = np.array([m.e ** (-0.1 * t) for t in interval_until_cut_tail_point])

points = np.c_[np.r_[x_until_cut_tail, -x_until_cut_tail], np.r_[y_until_cut_tail, -y_until_cut_tail],
               np.r_[z_until_cut_tail, -z_until_cut_tail]]
hull = ConvexHull(points)

for i in hull.simplices:
    ax.plot(points[i, 0], points[i, 1], points[i, 2], 'y:', linewidth=0.3)

ax.plot(m.e ** (alpha * cut_tail_point) * m.cos(beta * cut_tail_point),
        m.e ** (alpha * cut_tail_point) * m.sin(beta * cut_tail_point),
        'go', label=f'T_cut = {cut_tail_point:.4f}', linewidth=5)
ax.legend()
'''
