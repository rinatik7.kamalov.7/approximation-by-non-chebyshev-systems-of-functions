import math as m
from algorithms.one_dimensional_case.cut_tail_point.cut_tail_point_without_degeneracy \
    import CutTailPoint


def f_1(x):
    return m.exp(- 0.2 * x)


def f_2(x):
    return m.exp(- 0.5 * x)


a = [f_1, f_2]

modif_remez_alg_test_one = CutTailPoint([f_1, f_2], right=30)
modif_remez_alg_test_one.main_body()
