import math as m
import matplotlib.pyplot as plt
import numpy as np
from pathlib import Path
from scipy.spatial import ConvexHull

from algorithms.one_dimensional_case.cut_tail_point.cut_tail_point_without_degeneracy \
    import CutTailPoint


def f_1(x):
    return m.exp(- 0.1 * x) * m.sin(0.3 * x)


def f_2(x):
    return m.exp(- 0.1 * x) * m.cos(0.3 * x)


def f_3(x):
    return m.exp(-0.1 * x)


modif_remez_alg_test_one = CutTailPoint([f_1, f_2, f_3], Path(__file__).stem, right=20)
cut_tail_point = modif_remez_alg_test_one.main_body()
print(f"Точка отрезания хвоста {cut_tail_point}")

'''Let us draw trajectory of our 3D system.'''
interval = np.arange(0, 100, 0.05)
x = np.array([m.e ** (-0.1 * t) * m.sin(0.3 * t) for t in interval])
y = np.array([m.e ** (-0.1 * t) * m.cos(0.3 * t) for t in interval])
z = np.array([m.e ** (-0.1 * t) for t in interval])

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.set_title("Symmetrized trajectory of system $x^{'}=Ax,x(0)=(0,1,1)^T$")
ax.set_xlabel('$x_1$')
ax.set_ylabel('$x_2$')
ax.set_zlabel('$x_3$')
ax.grid(color='b', ls='-.', lw=0.25)
ax.plot(x, y, z, 'b', label='the trajectory $x(t)$', linewidth=2)
ax.plot(-x, -y, -z, 'g', label='the symmetric trajectory $-x(t)$', linewidth=2 )

interval_until_cut_tail_point = np.arange(0, cut_tail_point, 0.05)
x_until_cut_tail = np.array([m.e ** (-0.1 * t) * m.sin(0.3 * t) for t in interval_until_cut_tail_point])
y_until_cut_tail = np.array([m.e ** (-0.1 * t) * m.cos(0.3 * t) for t in interval_until_cut_tail_point])
z_until_cut_tail = np.array([m.e ** (-0.1 * t) for t in interval_until_cut_tail_point])

points = np.c_[np.r_[x_until_cut_tail, -x_until_cut_tail], np.r_[y_until_cut_tail, -y_until_cut_tail],
               np.r_[z_until_cut_tail, -z_until_cut_tail]]
hull = ConvexHull(points)

for i in hull.simplices:
    ax.plot(points[i, 0], points[i, 1], points[i, 2], color='lightblue', linewidth=0.1)
line, = ax.plot(0, 0, 0, 'mo', markersize=7)
ax.text(0.08, 0.07, 0.07, r'$(0,0,0)$', ha='left', va='center')

ax.plot(m.e ** (-0.1 * cut_tail_point) * m.sin(0.3 * cut_tail_point),
        m.e ** (-0.1 * cut_tail_point) * m.cos(0.3 * cut_tail_point),
        m.e ** (-0.1 * cut_tail_point), 'ro', label=r'$x(T_{cut})$', linewidth=5)
ax.text(m.e ** (-0.1 * cut_tail_point) * m.sin(0.3 * cut_tail_point) - 0.1,
        m.e ** (-0.1 * cut_tail_point) * m.cos(0.3 * cut_tail_point) + 0.2,
        m.e ** (-0.1 * cut_tail_point) + 0.2, r'$a=x(T_{cut})$', ha='left', va='center')

ax.plot(-m.e ** (-0.1 * cut_tail_point) * m.sin(0.3 * cut_tail_point),
        -m.e ** (-0.1 * cut_tail_point) * m.cos(0.3 * cut_tail_point),
        -m.e ** (-0.1 * cut_tail_point), 'ko', linewidth=5)
ax.plot(0, 1, 1, 'ko', linewidth=5)
ax.text(0.04, 1.03, 1.03, r'$x(0)$', ha='left', va='center')
ax.plot(0, -1, -1, 'ko', linewidth=5)
ax.text(-0.3, -1.03, -1.03, r'$-x(0)$', ha='left', va='center')
ax.legend(loc='upper left')
fig.savefig(Path(__file__).stem, bbox_inches='tight')
plt.show()

