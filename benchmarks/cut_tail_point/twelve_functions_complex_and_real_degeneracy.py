# FOR PAPER
import math as m
from pathlib import Path

from algorithms.one_dimensional_case.cut_tail_point.cut_tail_point_with_degeneracy \
    import CutTailPoint


def f_1(x):
    return m.exp(- 0.01 * x) * m.sin(-4 * x)


def f_2(x):
    return m.exp(- 0.01 * x) * m.cos(-4 * x)


def f_3(x):
    return m.exp(- 0.02 * x) * m.sin(2 * x)


def f_4(x):
    return m.exp(- 0.02 * x) * m.cos(2 * x)


def f_5(x):
    return m.exp(- 0.07 * x) * m.sin(2 * x)


def f_6(x):
    return m.exp(- 0.07 * x) * m.cos(2 * x)


def f_7(x):
    return m.exp(- 0.07 * x) * m.sin(1 * x)


def f_8(x):
    return m.exp(- 0.07 * x) * m.cos(1 * x)


def f_9(x):
    return m.exp(- 0.03 * x)


def f_10(x):
    return x * m.exp(-0.03 * x)


def f_11(x):
    return m.exp(-0.06 * x) * m.sin(-3 * x)


def f_12(x):
    return m.exp(-0.06 * x) * m.cos(-3 * x)


modif_remez_alg_test_one = CutTailPoint([f_1, f_2, f_3, f_4, f_5, f_6, f_7, f_8, f_9, f_10, f_11, f_12],
                                        Path(__file__).stem,
                                        right=200)
modif_remez_alg_test_one.main_body()
