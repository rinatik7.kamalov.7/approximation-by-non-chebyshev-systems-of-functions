import math as m
from algorithms.one_dimensional_case.cut_tail_point.cut_tail_point_without_degeneracy \
    import CutTailPoint


def f_1(x):
    return m.exp(- 0.1 * x) * m.sin(0.3 * x)


def f_2(x):
    return m.exp(- 0.1 * x) * m.cos(0.3 * x)


a = [f_1, f_2]

modif_remez_alg_test_one = CutTailPoint([f_1, f_2], right=20)
modif_remez_alg_test_one.main_body()
