# FOR PAPER
import math as m
from pathlib import Path

from algorithms.one_dimensional_case.cut_tail_point.cut_tail_point_with_degeneracy import CutTailPoint


def f_1(x):
    return m.exp(- 0.03 * x) * m.sin(2 * x)


def f_2(x):
    return m.exp(- 0.03 * x) * m.cos(2 * x)


def f_3(x):
    return m.exp(- 0.02 * x) * m.sin(1 * x)


def f_4(x):
    return m.exp(- 0.02 * x) * m.cos(1 * x)


def f_5(x):
    return m.exp(- 0.03 * x)


def f_6(x):
    return m.exp(- 0.01 * x)


modif_remez_alg_test_one = CutTailPoint([f_1, f_2, f_3, f_4, f_5, f_6], Path(__file__).stem,
                                        right=200)
modif_remez_alg_test_one.main_body()
# print(f"Файл {dict_with_upper_minus_lower}")


# Evaluation of rate of convergence
# rate_of_convergence(Path(r'C:\Users\rinat\PycharmProjects\Algorithms_for_approximation_by_non_Chebyshev_systems\results\cut_tail_point_degeneracy\ten_functions_complex_and_real_degeneracy'))
# print(f'Файл {dict_with_upper_minus_lower.parent}')
