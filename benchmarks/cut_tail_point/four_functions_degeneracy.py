# FOR PAPER
import math as m
from pathlib import Path
from algorithms.one_dimensional_case.cut_tail_point.cut_tail_point_with_degeneracy \
    import CutTailPoint


def f_1(x):
    return m.exp(- 0.03 * x) * m.sin(0.2 * x)


def f_2(x):
    return m.exp(- 0.03 * x) * m.cos(0.2 * x)


def f_3(x):
    return m.exp(-0.05 * x) * m.sin(0.6 * x)


def f_4(x):
    return m.exp(-0.05 * x) * m.cos(0.6 * x)


modif_remez_alg_test_one = CutTailPoint([f_1, f_2, f_3, f_4], Path(__file__).stem, right=200)
modif_remez_alg_test_one.main_body()