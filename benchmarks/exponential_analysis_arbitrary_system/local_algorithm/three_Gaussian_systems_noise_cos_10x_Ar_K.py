import math as m
from pathlib import Path

from algorithms.one_dimensional_case.exponential_analysis.two_phase_modified_Remez_algorithm_exponential_analysis import \
    ExponentialAnalysisSignal

sigma_squared = 9


# initial_shifts=[1.2, 2.2, 3.3],
def f_0(x):
    return m.exp(- (x - 1) ** 2 / sigma_squared) + 7 * m.exp(- (x - 5) ** 2 / sigma_squared) + \
        11 * m.exp(- (x - 7) ** 2 / sigma_squared) + m.sin(100 * x)


# Algorithm
cauchy_system = ExponentialAnalysisSignal('Gaussian', f_0, 3, sigma_squared,
                                          Path(__file__).parent.parts[-1],
                                          Path(__file__).stem,
                                          initial_shifts=[2., 5., 9.],
                                          eps_norm_difference=1e-4,
                                          left=0,
                                          right=10,
                                          step='Armijo_Kamalov',
                                          sigma_0=1,
                                          beta_0=0.1,
                                          error_Armijo=1e-6
                                          )
polynomial_of_best_approx = cauchy_system.main_body()

# Initial shifts:
