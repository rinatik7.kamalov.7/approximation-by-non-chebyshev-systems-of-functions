import math as m
from pathlib import Path

from algorithms.one_dimensional_case.exponential_analysis.global_optimization_analogue_monotone_basin_hopping_Cauchy_Gaussian \
    import ExponentialAnalysisSignalGlobal

sigma_squared = 4


def f_0(x):
    return 1 / (1 + (x - 1) ** 2 / sigma_squared) + 1 / (1 + (x - 4) ** 2 / sigma_squared) + \
        m.sin(2 * x) + m.cos(5 * x)


# Algorithm
cauchy_system = ExponentialAnalysisSignalGlobal('Cauchy', f_0, 2, sigma_squared,
                                                Path(__file__).parent.parts[-1],
                                                Path(__file__).stem,
                                                initial_shifts=[1.3, 8.2],
                                                niter_local=5,
                                                niter_global=10,
                                                max_norm_of_perturb=2,
                                                epsilon=1, left=0, right=10,
                                                step='theoretical')
polynomial_of_best_approx = cauchy_system.main_body()

# Initial shifts [1.3, 8.2]
