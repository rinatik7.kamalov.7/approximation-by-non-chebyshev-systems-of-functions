import math as m
from pathlib import Path

from algorithms.one_dimensional_case.exponential_analysis.two_phase_analog_Remez_algorithm_exponents import \
    ExponentialAnalysisSignal

sigma_squared = 4


def f_0(x):
    return 2 * m.exp(-2 * x) + 5 * m.exp(-3 * x) - m.exp(- x) * m.cos(2 * x) - \
           m.exp(-x) * m.sin(2 * x) + m.sin(30 * x)


# Algorithm
cauchy_system = ExponentialAnalysisSignal(f_0, 2, 1, Path(__file__).parent.parts[-1], Path(__file__).stem,
                                          initial_exponents={'real': [-0.87, -0.16],
                                                             'complex': [[-0.75, -4.48]]},
                                          eps_norm_difference=1e-1,
                                          epsilon=0.8, left=0, right=10)
polynomial_of_best_approx = cauchy_system.main_body()

# Initial exponents {'real': [-0.8790028405032562, -0.16323043460041298],
# 'complex': [[-0.7508990151163184, -4.487376051936963]]}
