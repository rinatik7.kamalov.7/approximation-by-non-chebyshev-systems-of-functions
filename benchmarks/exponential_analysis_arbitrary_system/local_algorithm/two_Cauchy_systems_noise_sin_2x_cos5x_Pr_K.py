import math as m
from pathlib import Path

from algorithms.one_dimensional_case.exponential_analysis.two_phase_modified_Remez_algorithm_exponential_analysis import \
    ExponentialAnalysisSignal

sigma_squared = 4


def f_0(x):
    return 1 / (1 + (x - 1) ** 2 / sigma_squared) + 1 / (1 + (x - 4) ** 2 / sigma_squared) + \
        m.sin(2 * x) + m.cos(5 * x)


# Algorithm
cauchy_system = ExponentialAnalysisSignal('Cauchy', f_0, 2, sigma_squared,
                                          Path(__file__).parent.parts[-1],
                                          Path(__file__).stem,
                                          initial_shifts=[1.3, 8.2],
                                          eps_norm_difference=1e-4,
                                          left=0,
                                          right=1,
                                          step='Protasov_Kamalov')
polynomial_of_best_approx = cauchy_system.main_body()

# Initial shifts [1.3, 8.2]
