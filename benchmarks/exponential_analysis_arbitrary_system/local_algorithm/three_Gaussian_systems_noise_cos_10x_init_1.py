import math as m
from pathlib import Path

from algorithms.one_dimensional_case.exponential_analysis.two_phase_modified_Remez_algorithm_exponential_analysis import \
    ExponentialAnalysisSignal

sigma_squared = 0.3

# initial_shifts=[1.2, 2.2, 3.3],
def f_0(x):
    return m.exp(- (x - 0.1) ** 2 / sigma_squared) + 7 * m.exp(- (x - 0.5) ** 2 / sigma_squared) + \
        11 * m.exp(- (x - 0.7) ** 2 / sigma_squared) + m.cos(10 * x)


# Algorithm
cauchy_system = ExponentialAnalysisSignal('Gaussian', f_0, 3, sigma_squared,
                                          Path(__file__).parent.parts[-1],
                                          Path(__file__).stem,
                                          initial_shifts=[0.1, 0.5, 0.9],
                                          eps_norm_difference=1e-4,
                                          epsilon=0.6,
                                          left=0,
                                          right=1,
                                          step='theoretical')
polynomial_of_best_approx = cauchy_system.main_body()

# Initial shifts:
