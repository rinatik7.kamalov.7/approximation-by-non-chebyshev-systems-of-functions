import math as m
from pathlib import Path

from algorithms.one_dimensional_case.exponential_analysis.two_phase_analog_Remez_algorithm_exponents import \
    ExponentialAnalysisSignal

left = 0
right = 10
sigma_squared = 4


def f_0(x):
    return 5 * m.cos(2 * x) - \
           3 * m.sin(2 * x) + m.sin(100 * x) + m.cos(50 * x) - 1.2 * m.sin(30 * x) + 0.7 * m.sin(12 * x)


# Algorithm
exp_system = ExponentialAnalysisSignal(f_0, 0, 1, Path(__file__).parent.parts[-1], Path(__file__).stem,
                                       initial_exponents={'real': [],
                                                          'complex': [[-0.5, 2.2]]},
                                       eps_norm_difference=1e-4,
                                       epsilon=0.8, left=left, right=right)
polynomial_of_best_approx = exp_system.main_body()

# Initial exponents {'real': [-0.8790028405032562, -0.16323043460041298],
# 'complex': [[-0.7508990151163184, -4.487376051936963]]}
