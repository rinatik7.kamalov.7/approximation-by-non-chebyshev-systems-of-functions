import math as m
from pathlib import Path

from algorithms.one_dimensional_case.exponential_analysis.two_phase_analog_Remez_algorithm_exponents import \
    ExponentialAnalysisSignal

sigma_squared = 4


def f_0(x):
    return 2 * m.exp(-2 * x) + 5 * m.exp(-3 * x) + m.sin(100 * x) + 2 * m.sin(2 * x)


# Algorithm
cauchy_system = ExponentialAnalysisSignal(f_0, 2, 0,
                                          Path(__file__).parent.parts[-1],
                                          Path(__file__).stem,
                                          initial_exponents={'real': [-2.8, -0.6],
                                                             'complex': []},
                                          eps_norm_difference=1e-4,
                                          left=0,
                                          right=1,
                                          step='Protasov_Kamalov')
polynomial_of_best_approx = cauchy_system.main_body()
