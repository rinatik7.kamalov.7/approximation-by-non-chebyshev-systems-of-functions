import math as m
from pathlib import Path

from algorithms.one_dimensional_case.exponential_analysis.two_phase_analog_Remez_algorithm_exponents import \
    ExponentialAnalysisSignal


def f_0(x):
    return 2 * m.exp(-0.2 * x) - 4 * m.exp(-0.8 * x) + m.cos(100 * x)


# Algorithm
cauchy_system = ExponentialAnalysisSignal(f_0, 2, 0,
                                          Path(__file__).parent.parts[-1],
                                          Path(__file__).stem,
                                          initial_exponents={'real': [-0.1, -0.5],
                                                             'complex': []},
                                          eps_norm_difference=1e-4,
                                          epsilon=0.8,
                                          left=0,
                                          right=1,
                                          step='simple'
                                          )
polynomial_of_best_approx = cauchy_system.main_body()
