import math as m
from pathlib import Path

from algorithms.one_dimensional_case.exponential_analysis.two_phase_modified_Remez_algorithm_exponential_analysis import \
    ExponentialAnalysisSignal

sigma_squared = 4


def f_0(x):
    return 1 / (1 + (x - 1) ** 2 / sigma_squared) + 1 / (1 + (x - 4) ** 2 / sigma_squared)


# Algorithm
cauchy_system = ExponentialAnalysisSignal('Cauchy', f_0, 2, sigma_squared,
                                          Path(__file__).parent.parts[-1],
                                          Path(__file__).stem,
                                          left=0,
                                          right=10,
                                          step='Armijo_Kamalov',
                                          sigma_0=1,
                                          beta_0=0.1,
                                          error_Armijo=1e-6)

polynomial_of_best_approx = cauchy_system.main_body()
