import math as m
from pathlib import Path

from algorithms.one_dimensional_case.exponential_analysis.two_phase_modified_Remez_algorithm_exponential_analysis import \
    ExponentialAnalysisSignal

sigma_squared = 9


def f_0(x):
    return 3 * 1 / (1 + (x - 1) ** 2 / sigma_squared) - 5 / (1 + (x - 5) ** 2 / sigma_squared) + \
           2 / (1 + (x - 9) ** 2 / sigma_squared) + m.sin(10 * x)


# Algorithm
cauchy_system = ExponentialAnalysisSignal('Cauchy', f_0, 3, sigma_squared,
                                          Path(__file__).parent.parts[-1],
                                          Path(__file__).stem,
                                          initial_shifts=[0., 6., 7.],
                                          eps_norm_difference=1e-4,
                                          left=0,
                                          right=10,
                                          step='Protasov_Kamalov'
                                          )
polynomial_of_best_approx = cauchy_system.main_body()


