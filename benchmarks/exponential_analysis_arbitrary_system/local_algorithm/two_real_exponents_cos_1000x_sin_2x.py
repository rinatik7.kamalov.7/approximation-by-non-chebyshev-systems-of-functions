import math as m
from pathlib import Path

from algorithms.one_dimensional_case.exponential_analysis.two_phase_analog_Remez_algorithm_exponents import \
    ExponentialAnalysisSignal

sigma_squared = 4


def f_0(x):
    return 2 * m.exp(-2 * x) + 3 * m.exp(-3 * x) + m.sin(200 * x)


# Algorithm
exponents = ExponentialAnalysisSignal(f_0, 2, 0,
                                      Path(__file__).parent.parts[-1],
                                      Path(__file__).stem,
                                      initial_exponents={'real': [-1., -4.],
                                                         'complex': []},
                                      eps_norm_difference=1e-4,
                                      epsilon=0.5,
                                      left=0,
                                      right=1,
                                      step='simple')
polynomial_of_best_approx = exponents.main_body()
