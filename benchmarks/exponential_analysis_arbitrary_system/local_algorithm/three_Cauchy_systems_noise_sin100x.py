import math as m
from pathlib import Path

from algorithms.one_dimensional_case.exponential_analysis.two_phase_modified_Remez_algorithm_exponential_analysis import \
    ExponentialAnalysisSignal

sigma_squared = 9

# initial_shifts=[2., 60., 82.]


def f_0(x):
    return 3 * 1 / (1 + (x - 10) ** 2 / sigma_squared) - 5 / (1 + (x - 50) ** 2 / sigma_squared) + \
           2 / (1 + (x - 90) ** 2 / sigma_squared) + m.sin(100 * x)


# Algorithm
cauchy_system = ExponentialAnalysisSignal('Cauchy', f_0, 3, sigma_squared,
                                          Path(__file__).parent.parts[-1],
                                          Path(__file__).stem,
                                          initial_shifts=[2., 60., 82.],
                                          eps_norm_difference=1e-4,
                                          epsilon=4.4,
                                          left=0,
                                          right=100,
                                          step='theoretical')
polynomial_of_best_approx = cauchy_system.main_body()


