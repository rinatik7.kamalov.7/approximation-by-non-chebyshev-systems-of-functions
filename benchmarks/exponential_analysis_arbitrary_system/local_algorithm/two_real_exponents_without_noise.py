import math as m
from pathlib import Path

from algorithms.one_dimensional_case.exponential_analysis.two_phase_analog_Remez_algorithm_exponents import \
    ExponentialAnalysisSignal

sigma_squared = 4


def f_0(x):
    return 2 * m.exp(-2 * x) + 5 * m.exp(-3 * x)


# Algorithm
cauchy_system = ExponentialAnalysisSignal(f_0, 2, 0,
                                          Path(__file__).parent.parts[-1],
                                          Path(__file__).stem,
                                          initial_exponents={'real': [-1.04, -0.68],
                                                             'complex': []},
                                          eps_norm_difference=1e-4,
                                          epsilon=0.8,
                                          left=0,
                                          right=1,
                                          step='simple')
polynomial_of_best_approx = cauchy_system.main_body()

# Initial exponents {'real': [-0.722288864027675, -1.231236629871248], 'complex': []}
