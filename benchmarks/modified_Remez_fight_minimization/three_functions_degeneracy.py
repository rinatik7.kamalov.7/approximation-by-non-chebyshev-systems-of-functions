import math as m
from pathlib import Path

from algorithms.one_dimensional_case.approximation_by_polynomials.modified_Remez_degeneracy_minimization \
    import ModifiedRemezAlgorithmFightWithDegeneracyMinimization


def f_0(x):
    return (0.001 * x) / (1 + x ** 2)


def f_1(x):
    return m.sin(2 * m.pi * x)


def f_2(x):
    return m.sin(2 * m.pi * x)


def f_3(x):
    return x * m.sin(2 * m.pi * x)


def f_4(x):
    return x * m.sin(2 * m.pi * x)


# Algorithm
modif_remez_alg_test_one = ModifiedRemezAlgorithmFightWithDegeneracyMinimization(f_0, [f_1, f_2, f_3, f_4],
                                                                                 Path(__file__).parent.parts[-1],
                                                                                 Path(__file__).stem,
                                                                                 left=0,
                                                                                 right=10)
upper_minus_lower = modif_remez_alg_test_one.main_body()
