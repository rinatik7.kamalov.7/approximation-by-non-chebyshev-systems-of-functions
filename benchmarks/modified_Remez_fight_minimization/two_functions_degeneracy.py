import math as m
from pathlib import Path

from algorithms.one_dimensional_case.approximation_by_polynomials.modified_Remez_degeneracy_minimization \
    import ModifiedRemezAlgorithmFightWithDegeneracyMinimization


def f_0(x):
    return (4 * x) / (1 + 4 * x ** 2)


def f_1(x):
    return m.sin(2 * m.pi * x)


def f_2(x):
    return m.cos(2 * m.pi * x)


# Algorithm
modif_remez_alg_test_one = ModifiedRemezAlgorithmFightWithDegeneracyMinimization(f_0, [f_1, f_2],
                                                                                 Path(__file__).parent.parts[-1],
                                                                                 Path(__file__).stem,
                                                                                 initial_points=[-0.5, 0, 0.3],
                                                                                 left=-1,
                                                                                 right=1)
upper_minus_lower = modif_remez_alg_test_one.main_body()
