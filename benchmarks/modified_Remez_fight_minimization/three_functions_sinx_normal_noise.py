import math as m
from pathlib import Path

from algorithms.one_dimensional_case.approximation_by_polynomials.modified_Remez_degeneracy_minimization import \
    ModifiedRemezAlgorithmFightWithDegeneracyMinimization


def f_0(x):
    return m.exp(- x) + 2 * m.exp(-0.3 * x)


def f_1(x):
    return m.exp(- x)


def f_2(x):
    return m.exp(-0.3 * x)


def f_3(x):
    return m.exp(-0.5 * x)


'''
# Modified_Remez_Algorithm
modif_remez_alg_test_one = ModifiedRemezAlgorithm(f_0, [f_1, f_2, f_3], Path(__file__).stem, right=10)
upper_minus_lower = modif_remez_alg_test_one.main_body()
print(f"Старая оцека {upper_minus_lower}")

# Least_Square_Method
least_square = LeastSquareMethod(f_0, [f_1, f_2, f_3], 10, right=10)
norm = least_square.main_body()
print(f"Новая оценка {norm}")
'''

# Evaluation of rate of convergence
modif_remez_alg_test_one = ModifiedRemezAlgorithmFightWithDegeneracyMinimization(f_0, [f_1, f_2, f_3],
                                                                                 Path(__file__).parent.parts[-1],
                                                                                 Path(__file__).stem, right=10,
                                                                                 noise='Gaussian',
                                                                                 mu_noise=0, sigma_noise=0.1)
upper_minus_lower = modif_remez_alg_test_one.main_body()
