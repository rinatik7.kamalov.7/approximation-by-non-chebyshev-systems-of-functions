import math as m
from pathlib import Path

from algorithms.one_dimensional_case.approximation_by_polynomials.modified_Remez_degeneracy_minimization \
    import ModifiedRemezAlgorithmFightWithDegeneracyMinimization


def f_0(x):
    return m.sin(x) + m.cos(x)


def f_1(x):
    return m.exp(- 0.5 * x) * m.sin(- 0.4 * x)


def f_2(x):
    return m.exp(- 0.5 * x) * m.cos(- 0.4 * x)


def f_3(x):
    return m.exp(- 0.1 * x) * m.sin(0.2 * x)


def f_4(x):
    return m.exp(- 0.1 * x) * m.cos(0.2 * x)


def f_5(x):
    return m.sin(- 0.3 * x)


def f_6(x):
    return m.cos(- 0.3 * x)


# Modified_Remez_Algorithm
modif_remez_alg_test_one = ModifiedRemezAlgorithmFightWithDegeneracyMinimization(f_0, [f_1, f_2, f_3, f_4, f_5, f_6],
                                                                                 Path(__file__).parent.parts[-1],
                                                                                 Path(__file__).stem, right=10)
upper_minus_lower = modif_remez_alg_test_one.main_body()
