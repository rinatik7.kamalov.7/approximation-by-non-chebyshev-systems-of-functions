from scipy.spatial import Delaunay


class ConvexHull:
    def __init__(self):
        self.hull = None
        self.point = None

    def point_in_hull(self, point, hull):
        self.point = point
        self.hull = hull
        hull = Delaunay(self.hull)
        return hull.find_simplex(self.point) >= 0
