import json
import random

import matplotlib.pyplot as plt
import numpy as np
from numpy.linalg import norm
from pathlib import Path

from algorithms.one_dimensional_case.exponential_analysis.two_phase_modified_Remez_algorithm_exponential_analysis \
    import ExponentialAnalysisSignal


class ExponentialAnalysisSignalGlobal:
    def __init__(self, type_of_signal, f_0, number_of_shifts, sigma_squared,
                 parent_folder, file_with_results, left=0, right=10, initial_shifts=None,
                 eps_norm_difference=1e-4, epsilon=0.5, step='Armijo_Kamalov', sigma_0=1,
                 beta_0=0.1, error_Armijo=1e-6, niter_local=10, min_norm_of_perturb=0.1,
                 max_norm_of_perturb=0.2, niter_global=10):
        self.exp_analysis_local_min = None
        self.type_of_signal = type_of_signal
        self.f_0 = f_0

        self.parent_folder = parent_folder
        self.dir_with_results = file_with_results
        self.file_with_results = file_with_results + '.json'

        self.left = left
        self.right = right

        self.sigma_squared = sigma_squared
        self.number_of_shifts = number_of_shifts
        self.initial_shifts = np.random.uniform(self.left, self.right, number_of_shifts) if initial_shifts is None \
            else np.array(initial_shifts)

        self.epsilon_norm_difference = eps_norm_difference
        self.epsilon = epsilon
        self.niter_local = niter_local

        self.niter_global = niter_global
        self.min_norm_of_perturb = min_norm_of_perturb
        self.max_norm_of_perturb = max_norm_of_perturb

        self.step = step
        self.step = step
        self.sigma_0 = sigma_0
        self.beta_0 = beta_0
        self.error_Armijo = error_Armijo

        self.dict_with_all_points = dict()
        self.best_point = dict()
        self.shifts_with_norm = dict()

    def main_body(self):
        self.best_point = {'point': 'the_worst_shifts', 'norm': np.inf}
        for _ in range(self.niter_global):
            # Here we define uniformly distributed random perturbation.
            random_shift = np.full(self.number_of_shifts, -1) + \
                           2 * np.random.rand(self.number_of_shifts)
            # Here we define the norm of our perturbation.
            norm_of_shift = random.uniform(self.min_norm_of_perturb, self.max_norm_of_perturb)
            self.shifts_with_norm[tuple(random_shift)] = norm_of_shift
            print(f'Global minimum contenders {self.dict_with_all_points}.')
            try:
                # Local algorithm.
                self.exp_analysis_local_min = ExponentialAnalysisSignal(self.type_of_signal, self.f_0,
                                                                        self.number_of_shifts, self.sigma_squared,
                                                                        self.parent_folder, self.file_with_results,
                                                                        initial_shifts=self.initial_shifts,
                                                                        epsilon=self.epsilon,
                                                                        left=self.left,
                                                                        right=self.right,
                                                                        loop="for",
                                                                        number_of_iter=self.niter_local,
                                                                        step=self.step,
                                                                        sigma_0=self.sigma_0,
                                                                        beta_0=self.beta_0,
                                                                        error_Armijo=self.error_Armijo)
                self.exp_analysis_local_min.main_body()
                self.dict_with_all_points[tuple(self.exp_analysis_local_min.shifts_for_best_approximation)] = \
                    self.exp_analysis_local_min.norm_for_best_approximation
            # We should specify this except later. It is in the case of Exception in the Local Algorithm.
            except:
                self.initial_shifts += norm_of_shift * (random_shift / norm(random_shift))
            else:
                if self.exp_analysis_local_min.norm_for_best_approximation < self.best_point['norm']:
                    """
                    self.best_point = {'point': tuple(self.exp_analysis_local_min.shifts_for_best_approximation),
                                       'norm': self.exp_analysis_local_min.norm_for_best_approximation}
                    """
                    self.initial_shifts = self.exp_analysis_local_min.shifts_for_best_approximation + norm_of_shift * \
                                          (random_shift / norm(random_shift))
                else:
                    self.initial_shifts += norm_of_shift * (random_shift / norm(random_shift))

        # Here we run the local algorithm for the best point with cycle "while".
        self.exp_analysis_local_min = ExponentialAnalysisSignal(self.type_of_signal, self.f_0,
                                                                self.number_of_shifts, self.sigma_squared,
                                                                self.parent_folder, self.file_with_results,
                                                                initial_shifts=self.initial_shifts,
                                                                eps_norm_difference=self.epsilon_norm_difference,
                                                                epsilon=self.epsilon,
                                                                left=self.left,
                                                                right=self.right,
                                                                loop='while',
                                                                step=self.step,
                                                                sigma_0=self.sigma_0,
                                                                beta_0=self.beta_0,
                                                                error_Armijo=self.error_Armijo
                                                                )
        self.exp_analysis_local_min.main_body()
        list_of_functions = [self.exp_analysis_local_min.functions(z)
                             for z in self.exp_analysis_local_min.shifts]
        self.best_point = {'point': tuple(self.exp_analysis_local_min.shifts_for_best_approximation),
                           'norm': self.exp_analysis_local_min.norm_for_best_approximation}
        print(f'The final point is obtained by the algorithm {self.best_point}.')

        # Let us write the results into the file.
        Path(Path.cwd().parent.parent.parent / "results" / "global_optimization_Cauchy_Gaussian" /
             self.dir_with_results).mkdir(parents=True, exist_ok=True)
        results = Path.cwd().parent.parent.parent / "results" / "global_optimization_Cauchy_Gaussian" / \
                  self.dir_with_results / self.file_with_results
        target = results.open("w", encoding="UTF-8")
        dict_with_results = {'all_points_with_error': {str(key): value
                                                       for key, value in self.dict_with_all_points.items()},
                             'best_point': self.best_point,
                             'all_shifts_with_norm': {str(key): value
                                                      for key, value in self.shifts_with_norm.items()}}
        print(f"Dictionary with results {dict_with_results}.")
        json.dump(dict_with_results, target, indent=4)

        # Figure with the function f_0 and polynomial of the best approximation.
        temp_address = results.stem + '_function_and_polynomial'
        fig = plt.figure()
        a = fig.add_axes([0, 0, 1, 1])
        a.set_title(r'Objective Function $f_0$ and Polynomial of Best Approximation $p$')
        a.set_xlabel('Points')
        a.set_ylabel(r'$f_0, p$')
        a.grid(color='b', ls='-.', lw=0.5)
        points = np.arange(self.left, self.right + 0.01, 0.01)
        values_of_f_0 = np.array([lambda x=y: self.f_0(x) for y in points])
        values_of_polynomial = (
            np.array([lambda x=y:
                      self.exp_analysis_local_min.polynom(
                          list_of_functions, self.exp_analysis_local_min.coefficients_first_system, x)
                      for y in points]))
        a.plot(points, [value() for value in values_of_f_0], 'r', label=r'$f_0$')
        a.plot(points, [value() for value in values_of_polynomial], 'b', label=r'$p$')
        a.set_xticks(np.arange(self.left, self.right + 0.01, (self.right - self.left) / 10))
        a.legend(loc='upper right')
        fig.savefig(results.parent / temp_address, bbox_inches='tight')

        # Figure with the alternance for final polynomial of the best approximation.
        temp_address = results.stem + '_alternance'
        fig = plt.figure()
        a = fig.add_axes([0, 0, 1, 1])
        a.set_title(r'Difference of Objective Function $f_0$ and Polynomial $p$')
        a.set_xlabel('Points')
        a.set_ylabel(r'$f_0-p$')
        a.grid(color='b', ls='-.', lw=0.5)
        points = np.arange(self.left, self.right + 0.01, 0.01)
        values = np.array([lambda x=y:
                           self.f_0(x) - self.exp_analysis_local_min.polynom(
                               list_of_functions, self.exp_analysis_local_min.coefficients_first_system, x)
                           for y in points])
        values_in_alternance_points = np.array(
            [lambda x=y: self.f_0(x) - self.exp_analysis_local_min.polynom(
                list_of_functions, self.exp_analysis_local_min.coefficients_first_system, x)
                                                for y in self.exp_analysis_local_min.alternance_points])
        a.plot(points, [value() for value in values], 'r')
        a.scatter(self.exp_analysis_local_min.alternance_points, [value() for value in values_in_alternance_points], color='black')
        a.set_xticks(np.arange(self.left, self.right + 0.01, (self.right - self.left) / 10))
        a.vlines(x=self.exp_analysis_local_min.alternance_points, ymin=0,
                 ymax=[value() for value in values_in_alternance_points],
                 color='green',
                 ls='--')
        a.hlines(y=0, xmin=self.left, xmax=self.right, color='blue')
        fig.savefig(results.parent / temp_address, bbox_inches='tight')

        # Figure with the rate of convergence.
        temp_address = results.stem + '_convergence'
        norm_system_minus_expanded = self.exp_analysis_local_min.norm_differences_first_system
        fig = plt.figure()
        a = fig.add_axes([0, 0, 1, 1])

        a.set_title("Convergence Rate")
        a.set_xlabel("Iteration number")
        a.set_ylabel(r"$\ln(h(c,0)-h(a,b))$")
        a.grid(color='b', ls='-.', lw=0.5)
        a.plot(np.arange(1, len(np.log(norm_system_minus_expanded)) + 1),
               np.log(norm_system_minus_expanded), 'ro-')
        a.set_xticks(np.arange(1, len(np.log(norm_system_minus_expanded)) + 1, 2))
        fig.savefig(results.parent / temp_address, bbox_inches='tight')

        # Figure with the rate of convergence of residuals.
        temp_address = results.stem + '_convergence_of_residuals'
        norm_f_minus_h_minus_h_opt = (self.exp_analysis_local_min.norm_differences_first_system -
                                      self.exp_analysis_local_min.norm_differences_first_system[-1])[:-1]
        norm_f_minus_h_minus_h_opt = [norm_f for norm_f in norm_f_minus_h_minus_h_opt if norm_f > 0]
        fig = plt.figure()
        a = fig.add_axes([0, 0, 1, 1])

        a.set_title("Convergence Rate")
        a.set_xlabel("Iteration number")
        a.set_ylabel(r"$\ln(h(c,0)-h^{*})$")
        a.grid(color='b', ls='-.', lw=0.5)
        a.plot(np.arange(1, len(np.log(norm_f_minus_h_minus_h_opt)) + 1),
               np.log(norm_f_minus_h_minus_h_opt), 'ro-')
        a.set_xticks(np.arange(1, len(np.log(norm_f_minus_h_minus_h_opt)) + 1, 2))

        fig.savefig(results.parent / temp_address, bbox_inches='tight')
