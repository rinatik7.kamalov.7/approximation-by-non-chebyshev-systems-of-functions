import json
import math as m
import matplotlib.pyplot as plt
import numpy as np
from copy import copy
from pathlib import Path
from scipy.optimize import dual_annealing

from algorithms.one_dimensional_case.approximation_by_polynomials.modified_Remez_degeneracy_minimization \
    import ModifiedRemezAlgorithmFightWithDegeneracyMinimization


class ExponentialAnalysisSignal:
    def __init__(self, type_of_signal, f_0, number_of_shifts, sigma_squared,
                 parent_folder, file_with_results, initial_shifts=None,
                 eps_norm_difference=1e-4, epsilon=0.5, left=0, right=1,
                 step='Protasov_Kamalov', loop='while', number_of_iter=None,
                 sigma_0=1, beta_0=0.1, error_Armijo=1e-6):

        self.parent_folder = parent_folder
        self.dir_with_results = file_with_results
        self.file_with_results = file_with_results + '.json'

        self.left = left
        self.right = right

        self.shifts_for_best_approximation = None
        self.coefficients_for_best_approximation = None
        self.norm_for_best_approximation = None

        self.number_of_iter = number_of_iter
        self.loop = loop

        self.sigma_squared = sigma_squared
        self.number_of_shifts = number_of_shifts
        self.shifts = np.random.uniform(self.left, self.right, number_of_shifts) if initial_shifts is None \
            else np.array(initial_shifts)
        self.initial_shifts = copy(self.shifts)

        self.type_of_signal = type_of_signal
        self.f_0 = f_0

        self.coefficients_first_system = None
        self.coefficients_second_system = None

        self.eps_norm_difference = eps_norm_difference
        self.epsilon = epsilon

        self.alternance_points = None
        self.alternance_points_expanded = None
        self.modified_Remez_coefficients_of_functions = None
        self.modified_Remez_shifts = None

        self.norm_first_system = np.inf
        self.norm_second_system = 0
        self.norm_differences_first_system = np.array([])
        self.norm_differences_expanded_system = np.array([])

        self.step = step
        self.lambda_ = 1
        self.r = 0
        self.sigma_0 = sigma_0
        self.beta_0 = beta_0
        self.error_Armijo = error_Armijo

        self.functions = None
        self.derivatives = None

    @staticmethod
    def polynom(functions, coefficients, x):
        return np.dot([f(x) for f in functions], coefficients)

    def uniform_norm_of_function(self, function):
        return m.sqrt(-dual_annealing(lambda x: - (function(x)) ** 2, [(self.left, self.right)]).fun)

    def generate_list_of_functions(self):
        if self.type_of_signal == "Gaussian":
            def functions(z):
                return lambda x: m.exp(- (x - z) ** 2 / self.sigma_squared)
        elif self.type_of_signal == "Cauchy":
            def functions(z):
                return lambda x: m.log(m.exp(1 / (1 + ((x - z) ** 2 / self.sigma_squared))))
        else:
            raise Exception(f"The type of approximation class is not defined.")
        self.functions = functions

    def generate_list_of_derivatives(self):
        if self.type_of_signal == "Gaussian":
            def derivatives(z):
                return lambda x: m.log(m.exp((2 / self.sigma_squared) *
                                             m.exp(- (x - z) ** 2 / self.sigma_squared)
                                             * (x - z)))
        elif self.type_of_signal == "Cauchy":
            def derivatives(z):
                return lambda x: m.log(m.exp((2 / self.sigma_squared) *
                                             ((1 + ((x - z) ** 2 / self.sigma_squared))
                                              ** (-2)) * (x - z)))
        else:
            raise Exception(f"The type of approximation class is not defined.")
        self.derivatives = derivatives

    def coefficients_of_system_and_expanded_and_shifts_obtained_from_lambda(self, _lambda):
        coefficients_of_system = ((1 - _lambda) * self.coefficients_first_system +
                                  _lambda * self.coefficients_second_system[:len(self.shifts)])
        coefficients_of_expanded_system = _lambda * self.coefficients_second_system[len(self.shifts):]
        delta = np.divide(coefficients_of_expanded_system, coefficients_of_system)
        return coefficients_of_system, coefficients_of_expanded_system, delta

    def super_upper_bound_independent_on_x_for_step(self, _lambda):
        coefficients_of_system, coefficients_of_expanded_system, delta = \
            self.coefficients_of_system_and_expanded_and_shifts_obtained_from_lambda(_lambda)

        upper_bound = max(np.abs((2 / self.sigma_squared) * ((2 / self.sigma_squared) *
                                                             (self.right - self.left) ** 2 - 1)),
                          (2 / self.sigma_squared)) * np.divide(np.square(coefficients_of_expanded_system),
                                                                np.abs(coefficients_of_system))
        return -_lambda * self.r + np.sum(upper_bound)

    def upper_bound_for_step(self, _lambda):
        coefficients_of_system, coefficients_of_expanded_system, delta = \
            self.coefficients_of_system_and_expanded_and_shifts_obtained_from_lambda(_lambda)

        temporary_shifts = self.shifts + delta

        # Here we define polynomial p_lambda.
        list_of_functions = [self.functions(z) for z in temporary_shifts]

        # print(f'Values of functions in shifts {[f(x) for f, x in zip(list_of_functions, temporary_shifts)]}')

        '''
        def p_lambda(x):
            return self.polynom(list_of_functions, coefficients_of_system, x)
        '''

        # Here we define polynomial p^hat_lambda.
        list_of_functions_expanded = [self.functions(z) for z in self.shifts]
        list_of_derivatives_of_functions = [self.derivatives(z)
                                            for z in self.shifts]
        list_of_functions_and_derivatives = list_of_functions_expanded + list_of_derivatives_of_functions

        '''
        def p_expanded_system(x):
            return self.polynom(list_of_functions_and_derivatives,
                                np.concatenate((coefficients_of_system,
                                                coefficients_of_expanded_system)), x)
        '''

        sup_norm_of_p_lambda_minus_p_expanded_system = (
            self.uniform_norm_of_function(lambda x: self.polynom(list_of_functions, coefficients_of_system, x) -
                                                    self.polynom(list_of_functions_and_derivatives,
                                                                 np.concatenate((coefficients_of_system,
                                                                                 coefficients_of_expanded_system)), x)))
        '''
        print(f'Current lambda  {_lambda}')
        print(f'Value in lambda {sup_norm_of_p_lambda_minus_p_expanded_system - _lambda * self.r}')
        '''

        return sup_norm_of_p_lambda_minus_p_expanded_system - _lambda * self.r

    def main_body_for_loop_for_or_while(self):
        list_of_functions = [self.functions(z) for z in self.shifts]
        self.modified_Remez_coefficients_of_functions = \
            ModifiedRemezAlgorithmFightWithDegeneracyMinimization(
                self.f_0, list_of_functions, self.parent_folder,
                left=self.left, right=self.right)
        self.modified_Remez_coefficients_of_functions.main_body()
        self.coefficients_first_system = \
            self.modified_Remez_coefficients_of_functions.coefficients_of_the_best_polynom
        self.norm_first_system = self.modified_Remez_coefficients_of_functions.upper_bound
        self.norm_differences_first_system = np.append(self.norm_differences_first_system,
                                                       self.norm_first_system)

        # The best polynomial w.r.t. the system of Gaussian functions and their derivatives.
        list_of_derivatives_of_functions = [self.derivatives(z)
                                            for z in self.shifts]
        list_of_functions_and_derivatives = list_of_functions + \
                                            list_of_derivatives_of_functions
        self.modified_Remez_shifts = ModifiedRemezAlgorithmFightWithDegeneracyMinimization(
            self.f_0, list_of_functions_and_derivatives, self.parent_folder,
            left=self.left, right=self.right)
        self.modified_Remez_shifts.main_body()
        self.coefficients_second_system = self.modified_Remez_shifts.coefficients_of_the_best_polynom
        self.norm_second_system = self.modified_Remez_shifts.upper_bound
        self.norm_differences_expanded_system = np.append(self.norm_differences_expanded_system,
                                                          self.norm_second_system)

        # Now we choose the step _lambda.
        self.r = self.norm_first_system - self.norm_second_system
        if self.step == 'simple':
            lambda_ = min(min(self.epsilon / abs(b_k) for b_k in self.coefficients_second_system),
                          1)
            delta = np.divide(lambda_ * self.coefficients_second_system[len(self.shifts):],
                              (1 - lambda_) * self.coefficients_first_system + lambda_ *
                              self.coefficients_second_system[:len(self.shifts)])
            self.shifts += delta

        if self.step == 'theoretical':
            lambda_ = 1
            norm_first_system = self.norm_first_system
            while lambda_ >= 0.1:
                delta = np.divide(lambda_ * self.coefficients_second_system[len(self.shifts):],
                                  (1 - lambda_) * self.coefficients_first_system + lambda_ *
                                  self.coefficients_second_system[:len(self.shifts)])

                # Check if the norm is reducing.
                shifts_check = np.copy(self.shifts) + delta
                list_of_functions = [self.functions(z)
                                     for z in shifts_check]
                self.modified_Remez_coefficients_of_functions = \
                    ModifiedRemezAlgorithmFightWithDegeneracyMinimization(
                        self.f_0, list_of_functions, self.parent_folder,
                        left=self.left, right=self.right)
                self.modified_Remez_coefficients_of_functions.main_body()
                self.coefficients_first_system = \
                    self.modified_Remez_coefficients_of_functions.coefficients_of_the_best_polynom
                norm_first_system = \
                    self.modified_Remez_coefficients_of_functions.upper_bound
                lambda_ -= 0.1

                if self.norm_first_system > norm_first_system:
                    self.shifts += delta
                    break

            if norm_first_system >= self.norm_first_system:
                lambda_ = min(min(self.epsilon / abs(b_k) for b_k in self.coefficients_second_system),
                              1)
                delta = np.divide(lambda_ * self.coefficients_second_system[len(self.shifts):],
                                  (1 - lambda_) * self.coefficients_first_system + lambda_ *
                                  self.coefficients_second_system[:len(self.shifts)])
                self.shifts += delta

        if self.step == 'Protasov_Kamalov':
            self.lambda_ = dual_annealing(self.super_upper_bound_independent_on_x_for_step, [(0, 1)]).x
            '''
            print(f'Lambda_optimal {self.lambda_}')
            print(f"Value in optimal {self.super_upper_bound_independent_on_x_for_step(self.lambda_)}")
            '''

        if self.step == 'Armijo_Kamalov':
            self.lambda_ = self.sigma_0
            while self.upper_bound_for_step(self.lambda_) > 0:
                self.lambda_ *= self.beta_0

        # Here we get new shifts.
        delta = self.coefficients_of_system_and_expanded_and_shifts_obtained_from_lambda(self.lambda_)[-1]
        self.shifts += delta

    def print_in_console(self):
        print(f"Coefficients of polynomial of best approximation  {self.coefficients_first_system};\n"
              f"Difference of norm f and first approximation {self.norm_first_system};\n"
              f"Current shifts: {self.shifts};\n"
              f"Coefficients of expanded system {self.coefficients_second_system};\n"
              f"Difference of norm f and second approximation {self.norm_second_system}.\n")

    def main_body(self):
        self.generate_list_of_functions()
        self.generate_list_of_derivatives()
        if self.loop == "while":
            while self.norm_first_system - self.norm_second_system > self.eps_norm_difference:
                self.main_body_for_loop_for_or_while()
                self.print_in_console()

        if self.loop == "for":
            for _ in range(self.number_of_iter):
                self.main_body_for_loop_for_or_while()
                if self.parent_folder == "local_algorithm":
                    self.print_in_console()

        # Here we define the coefficients of the best polynomial.
        list_of_functions = [self.functions(z) for z in self.shifts]
        self.modified_Remez_coefficients_of_functions = \
            ModifiedRemezAlgorithmFightWithDegeneracyMinimization(
                self.f_0, list_of_functions, self.parent_folder, left=self.left, right=self.right)
        self.modified_Remez_coefficients_of_functions.main_body()
        self.coefficients_first_system = \
            self.modified_Remez_coefficients_of_functions.coefficients_of_the_best_polynom
        self.alternance_points = self.modified_Remez_coefficients_of_functions.points
        self.norm_first_system = self.modified_Remez_coefficients_of_functions.upper_bound
        self.norm_differences_first_system = np.append(self.norm_differences_first_system,
                                                       self.norm_first_system)

        # Final shifts and coefficients of the best approximation.
        self.norm_for_best_approximation = self.modified_Remez_coefficients_of_functions.upper_bound
        self.coefficients_for_best_approximation = \
            copy(self.modified_Remez_coefficients_of_functions.coefficients_of_the_best_polynom)
        self.shifts_for_best_approximation = copy(self.shifts)

        # Here we define the coefficients of the best polynomial by expanded system.
        list_of_derivatives_of_Gaussian_functions = [self.derivatives(z) for z in self.shifts]
        self.modified_Remez_shifts = ModifiedRemezAlgorithmFightWithDegeneracyMinimization(
            self.f_0, list_of_functions + list_of_derivatives_of_Gaussian_functions, self.parent_folder,
            left=self.left, right=self.right)
        self.modified_Remez_shifts.main_body()
        self.coefficients_second_system = self.modified_Remez_shifts.coefficients_of_the_best_polynom
        self.alternance_points_expanded = self.modified_Remez_shifts.points
        self.norm_second_system = self.modified_Remez_shifts.upper_bound
        self.norm_differences_expanded_system = np.append(self.norm_differences_expanded_system,
                                                          self.norm_second_system)

        if self.parent_folder == "local_algorithm":
            # Let us write the results into the file.
            Path(Path.cwd().parent.parent.parent / "results" / "exponential_analysis" / "local_algorithm" /
                 self.dir_with_results).mkdir(parents=True, exist_ok=True)
            results = Path.cwd().parent.parent.parent / "results" / "exponential_analysis" / "local_algorithm" / \
                      self.dir_with_results / self.file_with_results
            target = results.open("w", encoding="UTF-8")
            dict_with_results = {'initial_point': self.initial_shifts.tolist(),
                                 'norm_f_minus_h(0,c)': list(self.norm_differences_first_system),
                                 'difference_norm_given_system_minus_expanded':
                                     list(self.norm_differences_first_system - self.norm_differences_expanded_system),
                                 'coefficients_of_the_best_polynom': list(self.coefficients_first_system),
                                 'shifts': list(self.shifts)}
            json.dump(dict_with_results, target, indent=4)

            # Figure with the function f_0 and polynomial of the best approximation.
            temp_address = results.stem + '_function_and_polynomial'
            fig = plt.figure()
            a = fig.add_axes([0, 0, 1, 1])
            a.set_title(r'Objective Function $f_0$ and Polynomial of Best Approximation $p$')
            a.set_xlabel('Points')
            a.set_ylabel(r'$f_0, p$')
            a.grid(color='b', ls='-.', lw=0.5)
            points = np.arange(self.left, self.right + 0.01, 0.01)
            values_of_f_0 = np.array([lambda x=y: self.f_0(x) for y in points])
            values_of_polynomial = np.array([lambda x=y: self.polynom(list_of_functions,
                                                                      self.coefficients_first_system, x)
                                             for y in points])
            a.plot(points, [value() for value in values_of_f_0], 'r', label=r'$f_0$')
            a.plot(points, [value() for value in values_of_polynomial], 'b', label=r'$p$')
            a.set_xticks(np.arange(self.left, self.right + 0.01, (self.right - self.left) / 10))
            a.legend(loc='upper right')
            fig.savefig(results.parent / temp_address, bbox_inches='tight')

            # Figure with the alternance for final polynomial of the best approximation.
            temp_address = results.stem + '_alternance'
            fig = plt.figure()
            a = fig.add_axes([0, 0, 1, 1])
            a.set_title(r'Difference of Objective Function $f_0$ and Polynomial $p$')
            a.set_xlabel('Points')
            a.set_ylabel(r'$f_0-p$')
            a.grid(color='b', ls='-.', lw=0.5)
            points = np.arange(self.left, self.right + 0.01, 0.01)
            values = np.array([lambda x=y: self.f_0(x) - self.polynom(list_of_functions,
                                                                      self.coefficients_first_system, x)
                               for y in points])
            values_in_alternance_points = np.array([lambda x=y: self.f_0(x) - self.polynom(list_of_functions,
                                                                                           self.coefficients_first_system,
                                                                                           x)
                                                    for y in self.alternance_points])
            a.plot(points, [value() for value in values], 'r')
            a.scatter(self.alternance_points, [value() for value in values_in_alternance_points], color='black')
            a.set_xticks(np.arange(self.left, self.right + 0.01, (self.right - self.left) / 10))
            a.vlines(x=self.alternance_points, ymin=0, ymax=[value() for value in values_in_alternance_points],
                     color='green',
                     ls='--')
            a.hlines(y=0, xmin=self.left, xmax=self.right, color='blue')
            fig.savefig(results.parent / temp_address, bbox_inches='tight')

            # Figure with the rate of convergence.
            temp_address = results.stem + '_convergence'
            norm_system_minus_expanded = self.norm_differences_first_system
            fig = plt.figure()
            a = fig.add_axes([0, 0, 1, 1])

            a.set_title("Convergence Rate")
            a.set_xlabel("Iteration number")
            a.set_ylabel(r"$\ln(h(c,0)-h(a,b))$")
            a.grid(color='b', ls='-.', lw=0.5)
            a.plot(np.arange(1, len(np.log(norm_system_minus_expanded)) + 1),
                   np.log(norm_system_minus_expanded), 'ro-')
            a.set_xticks(np.arange(1, len(np.log(norm_system_minus_expanded)) + 1, 2))
            fig.savefig(results.parent / temp_address, bbox_inches='tight')

            # Figure with the rate of convergence of residuals.
            temp_address = results.stem + '_convergence_of_residuals'
            norm_f_minus_h_minus_h_opt = (self.norm_differences_first_system -
                                          self.norm_differences_first_system[-1])[:-1]
            norm_f_minus_h_minus_h_opt = [norm_f for norm_f in norm_f_minus_h_minus_h_opt if norm_f > 0]
            fig = plt.figure()
            a = fig.add_axes([0, 0, 1, 1])

            a.set_title("Convergence Rate")
            a.set_xlabel("Iteration number")
            a.set_ylabel(r"$\ln(h(c,0)-h^{*})$")
            a.grid(color='b', ls='-.', lw=0.5)
            a.plot(np.arange(1, len(np.log(norm_f_minus_h_minus_h_opt)) + 1),
                   np.log(norm_f_minus_h_minus_h_opt), 'ro-')
            a.set_xticks(np.arange(1, len(np.log(norm_f_minus_h_minus_h_opt)) + 1, 2))

            fig.savefig(results.parent / temp_address, bbox_inches='tight')
            return results
