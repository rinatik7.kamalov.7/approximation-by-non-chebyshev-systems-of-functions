import json
import math as m
import numpy as np
from numpy.linalg import solve
import matplotlib.pyplot as plt
from pathlib import Path
from scipy.optimize import dual_annealing

from algorithms.one_dimensional_case.approximation_by_polynomials.modified_Remez_degeneracy_minimization \
    import ModifiedRemezAlgorithmFightWithDegeneracyMinimization


class ExponentialAnalysisSignal:
    def __init__(self, f_0, number_of_real_exp, number_of_complex_exp,
                 parent_folder, file_with_results, initial_exponents=None,
                 eps_norm_difference=1e-4, epsilon=0.3, left=0, right=1,
                 step='Protasov_Kamalov', loop='while', number_of_iter=None,
                 sigma_0=1, beta_0=0.1):

        self.parent_folder = parent_folder
        self.dir_with_results = file_with_results
        self.file_with_results = file_with_results + '.json'

        self.left = left
        self.right = right

        self.exponents_for_best_approximation = None
        self.coefficients_for_best_approximation = None
        self.norm_for_best_approximation = None

        self.number_of_iter = number_of_iter
        self.loop = loop

        self.number_of_real_exp = number_of_real_exp
        self.number_of_complex_exp = number_of_complex_exp
        self.exponents = {'real': np.random.uniform(-5, 0, self.number_of_real_exp),
                          'complex': np.column_stack((np.random.uniform(-5, 0, self.number_of_complex_exp),
                                                      np.random.uniform(-5, 5, self.number_of_complex_exp)))} \
            if initial_exponents is None \
            else {key: np.array(value) for key, value in initial_exponents.items()}
        self.initial_exponents = {key: value.tolist() if not isinstance(value, list) else value
                                  for (key, value) in self.exponents.items()}

        self.f_0 = f_0

        self.coefficients_first_system = None

        self.coefficients_of_real_exponents = None
        self.coefficients_of_complex_exponents = None
        self.coefficients_of_real_exp_expanded_system = None
        self.coefficients_of_complex_exp_expanded_system = None
        self.coefficients_of_real_exp_mult_by_x_expanded_system = None
        self.coefficients_of_complex_exp_mult_by_x_expanded_system = None
        self.expanded_system = None

        self.eps_norm_difference = eps_norm_difference
        self.epsilon = epsilon

        self.alternance_points = None
        self.alternance_points_expanded = None
        self.modified_Remez_coefficients_of_exponents = None
        self.modified_Remez_exponents = None

        self.norm_first_system = np.inf
        self.norm_second_system = 0
        self.norm_differences_first_system = np.array([])
        self.norm_differences_expanded_system = np.array([])

        self.step = step
        self.lambda_ = 1
        self.r = 0
        self.sigma_0 = sigma_0
        self.beta_0 = beta_0

    @staticmethod
    def polynom(functions, coefficients, x):
        return np.dot([f(x) for f in functions], coefficients)

    @staticmethod
    def generate_list_of_real_exponents(z):
        return lambda x: m.exp(z * x)

    @staticmethod
    def generate_list_of_complex_exponents(z):
        return lambda x: m.exp(z[0] * x) * m.sin(z[1] * x), \
            lambda x: m.exp(z[0] * x) * m.cos(z[1] * x)

    @staticmethod
    def generate_list_of_real_exponents_multiplied_by_x(z):
        return lambda x: x * m.exp(z * x)

    @staticmethod
    def generate_list_of_complex_exponents_multiplied_by_x(z):
        return lambda x: x * m.exp(z[0] * x) * m.sin(z[1] * x), \
            lambda x: x * m.exp(z[0] * x) * m.cos(z[1] * x)

    def coefficients_of_exponents_and_expanded_and_shifts_of_exponents_obtained_from_lambda(self, _lambda):
        # Real exponents.
        coefficients_of_real_exponents = ((1 - _lambda) * self.coefficients_of_real_exponents +
                                          _lambda * self.coefficients_of_real_exp_expanded_system)
        coefficients_of_real_exponents_expanded = (_lambda * self.coefficients_of_real_exp_mult_by_x_expanded_system)
        delta_real = np.divide(coefficients_of_real_exponents_expanded, coefficients_of_real_exponents)

        # Complex exponents.
        coefficients_of_complex_exponents = ((1 - _lambda) * self.coefficients_of_complex_exponents +
                                             _lambda * self.coefficients_of_complex_exp_expanded_system)
        coefficients_of_complex_exponents_expanded = (_lambda
                                                      * self.coefficients_of_complex_exp_mult_by_x_expanded_system)
        delta_complex = np.empty((0, 2))  # It is an empty array, but it has the proper dimensionality.
        for alpha_1, alpha_2, beta_1, beta_2 in zip(coefficients_of_complex_exponents[0::2],
                                                    coefficients_of_complex_exponents[1::2],
                                                    coefficients_of_complex_exponents_expanded[0::2],
                                                    coefficients_of_complex_exponents_expanded[1::2]):
            matrix_a_lin_system = np.array([[alpha_1, -alpha_2], [alpha_2, alpha_1]])
            vector_b = np.array([beta_1, beta_2])
            delta_complex = np.concatenate((delta_complex, np.array([solve(matrix_a_lin_system, vector_b)])),
                                           axis=0)
        return (coefficients_of_real_exponents, coefficients_of_real_exponents_expanded,
                coefficients_of_complex_exponents, coefficients_of_complex_exponents_expanded,
                delta_real, delta_complex)

    def super_upper_bound_independent_on_x_for_step(self, _lambda):
        abs_value_of_real_coefficients = []
        norm_of_real_exponents = []
        sup_norm_for_real_part = []
        complex_abs_value_of_complex_coefficients = []
        norm_of_complex_exponents = []
        sup_norm_for_complex_part = []

        (coefficients_of_real_exponents, coefficients_of_real_exponents_expanded, coefficients_of_complex_exponents,
         coefficients_of_complex_exponents_expanded, delta_real, delta_complex) = \
            self.coefficients_of_exponents_and_expanded_and_shifts_of_exponents_obtained_from_lambda(_lambda)

        # Real exponents.
        if self.exponents["real"].size != 0:
            abs_value_of_real_coefficients = np.abs(coefficients_of_real_exponents)
            norm_of_real_exponents = np.maximum(np.exp(self.exponents['real'] * self.right),
                                                np.exp(self.exponents['real'] * self.left))
            sup_norm_for_real_part = np.maximum(np.exp(delta_real * self.right) - delta_real * self.right
                                                - 1,
                                                np.exp(delta_real * self.left) - delta_real * self.left - 1)

        # Complex exponents.
        if self.exponents["complex"].size != 0:
            complex_abs_value_of_complex_coefficients = np.sqrt([k_1 ** 2 + k_2 ** 2 for k_1, k_2 in
                                                                 zip(coefficients_of_complex_exponents[0::2],
                                                                     coefficients_of_complex_exponents[1::2])])
            norm_of_complex_exponents = np.maximum(np.exp(self.exponents['complex'][:, 0] * self.right),
                                                   np.exp(self.exponents['complex'][:, 0] * self.left))

            # delta_complex[:, 0] = delta_1, delta_complex[:, 1] = delta_2
            sup_norm_for_complex_part = (np.maximum.reduce([np.abs(np.exp(delta_complex[:, 0] * self.right) *
                                                                   np.cos(delta_complex[:, 1] * self.right) -
                                                                   delta_complex[:, 0] * self.right - 1),

                                                            np.abs(np.exp(delta_complex[:, 0] * self.left) *
                                                                   np.cos(delta_complex[:, 1] * self.left) -
                                                                   delta_complex[:, 0] * self.left - 1)]) +

                                         np.maximum.reduce([np.abs(np.exp(delta_complex[:, 0] * self.right) *
                                                                   np.sin(delta_complex[:, 1] * self.right) -
                                                                   delta_complex[:, 1] * self.right),

                                                            np.abs(np.exp(delta_complex[:, 0] * self.left) *
                                                                   np.sin(delta_complex[:, 1] * self.left) -
                                                                   delta_complex[:, 1] * self.left),

                                                            ]))
        return (-_lambda * self.r +
                np.sum(abs_value_of_real_coefficients * norm_of_real_exponents * sup_norm_for_real_part
                       if self.exponents["real"].size != 0 else 0) +
                np.sum(
                    complex_abs_value_of_complex_coefficients * norm_of_complex_exponents * sup_norm_for_complex_part
                    if self.exponents["complex"].size != 0 else 0)
                )

    def upper_bound_for_step(self, _lambda):
        (coefficients_of_real_exponents, coefficients_of_real_exponents_expanded, coefficients_of_complex_exponents,
         coefficients_of_complex_exponents_expanded, delta_real, delta_complex) = \
            self.coefficients_of_exponents_and_expanded_and_shifts_of_exponents_obtained_from_lambda(_lambda)

        temporary_real_exponents = self.exponents['real'] + delta_real
        temporary_complex_exponents = np.array([complex_exp + delta_complex_exp for complex_exp, delta_complex_exp in
                                                zip(self.exponents['complex'], delta_complex)])

        # Here we define polynomial p_lambda.
        nested_list_of_complex_exponents = [self.generate_list_of_complex_exponents(z) for z in
                                            temporary_complex_exponents]
        list_of_exponents = [self.generate_list_of_real_exponents(z) for z in temporary_real_exponents] + \
                            [exp for sublist in nested_list_of_complex_exponents for exp in sublist]

        def p_lambda(x):
            return self.polynom(list_of_exponents,
                                np.concatenate((coefficients_of_real_exponents, coefficients_of_complex_exponents)), x)

        # Here we define polynomial p^hat_lambda.
        nested_list_of_complex_exponents_expanded = [self.generate_list_of_complex_exponents(z) for z in
                                                     self.exponents['complex']]
        nested_list_of_complex_exp_mult_by_x = [self.generate_list_of_complex_exponents_multiplied_by_x(z)
                                                for z in self.exponents['complex']]
        functions_of_expanded_system = [self.generate_list_of_real_exponents(z) for z in self.exponents['real']] + \
                                       [self.generate_list_of_real_exponents_multiplied_by_x(z)
                                        for z in self.exponents['real']] + \
                                       [exp for sublist in nested_list_of_complex_exponents_expanded for exp in sublist] + \
                                       [exp for sublist in nested_list_of_complex_exp_mult_by_x for exp in sublist]

        def p_expanded_system(x):
            return self.polynom(functions_of_expanded_system,
                                np.concatenate((coefficients_of_real_exponents,
                                                coefficients_of_real_exponents_expanded,
                                                coefficients_of_complex_exponents,
                                                coefficients_of_complex_exponents_expanded)), x)

        sup_norm_of_p_lambda_minus_p_expanded_system = (
            self.uniform_norm_of_function(lambda x: p_lambda(x) - p_expanded_system(x)))
        print(f'Current lambda  {_lambda}')
        print(f'Value in lambda {sup_norm_of_p_lambda_minus_p_expanded_system - _lambda * self.r}')

        return sup_norm_of_p_lambda_minus_p_expanded_system - _lambda * self.r

    def uniform_norm_of_function(self, function):
        return m.sqrt(-dual_annealing(lambda x: - (function(x)) ** 2, [(self.left, self.right)]).fun)

    def main_body_for_loop_for_or_while(self):
        # The best polynom w.r.t. the system of exponents.
        nested_list_of_complex_exponents = [self.generate_list_of_complex_exponents(z) for z in
                                            self.exponents['complex']]
        list_of_exponents = [self.generate_list_of_real_exponents(z) for z in self.exponents['real']] + \
                            [exp for sublist in nested_list_of_complex_exponents for exp in sublist]
        self.modified_Remez_coefficients_of_exponents = ModifiedRemezAlgorithmFightWithDegeneracyMinimization(
            self.f_0, list_of_exponents, self.parent_folder, left=self.left, right=self.right)
        self.modified_Remez_coefficients_of_exponents.main_body()
        self.coefficients_of_real_exponents = \
            self.modified_Remez_coefficients_of_exponents.coefficients_of_the_best_polynom[
            :len(self.exponents['real'])]
        self.coefficients_of_complex_exponents = \
            self.modified_Remez_coefficients_of_exponents.coefficients_of_the_best_polynom[
            len(self.exponents['real']):]
        self.norm_first_system = self.modified_Remez_coefficients_of_exponents.upper_bound
        self.norm_differences_first_system = np.append(self.norm_differences_first_system, self.norm_first_system)

        # The best polynom w.r.t. the system of exponents and their derivatives.
        nested_list_of_complex_exp_mult_by_x = [self.generate_list_of_complex_exponents_multiplied_by_x(z)
                                                for z in self.exponents['complex']]
        functions_of_expanded_system = [self.generate_list_of_real_exponents(z) for z in self.exponents['real']] + \
                                       [self.generate_list_of_real_exponents_multiplied_by_x(z)
                                        for z in self.exponents['real']] + \
                                       [exp for sublist in nested_list_of_complex_exponents for exp in sublist] + \
                                       [exp for sublist in nested_list_of_complex_exp_mult_by_x for exp in sublist]
        self.modified_Remez_exponents = ModifiedRemezAlgorithmFightWithDegeneracyMinimization(
            self.f_0, functions_of_expanded_system, self.parent_folder, left=self.left, right=self.right)
        self.modified_Remez_exponents.main_body()
        self.coefficients_of_real_exp_expanded_system = \
            self.modified_Remez_exponents.coefficients_of_the_best_polynom[:len(self.exponents['real'])]
        self.coefficients_of_real_exp_mult_by_x_expanded_system = \
            self.modified_Remez_exponents.coefficients_of_the_best_polynom[len(self.exponents['real']):
                                                                           2 * len(self.exponents['real']):]
        self.coefficients_of_complex_exp_expanded_system = \
            self.modified_Remez_exponents.coefficients_of_the_best_polynom[2 * len(self.exponents['real']):
                                                                           2 * len(self.exponents['real']) +
                                                                           2 * len(self.exponents['complex'])]
        self.coefficients_of_complex_exp_mult_by_x_expanded_system = \
            self.modified_Remez_exponents.coefficients_of_the_best_polynom[2 * len(self.exponents['real']) +
                                                                           2 * len(self.exponents['complex']):]
        self.norm_second_system = self.modified_Remez_exponents.upper_bound
        self.norm_differences_expanded_system = np.append(self.norm_differences_expanded_system,
                                                          self.norm_second_system)

        # Now we choose the step _lambda.
        self.r = self.norm_first_system - self.norm_second_system
        if self.step == 'simple':
            self.lambda_ = min(min(self.epsilon / abs(b_k) for b_k in
                                   np.concatenate((self.coefficients_of_real_exp_mult_by_x_expanded_system,
                                                   self.coefficients_of_complex_exp_mult_by_x_expanded_system))), 1)

        if self.step == 'Protasov_Kamalov':
            self.lambda_ = dual_annealing(self.super_upper_bound_independent_on_x_for_step, [(0, 1)]).x
            print(f'Lambda_optimal {self.lambda_}')
            print(f"Value in optimal {self.super_upper_bound_independent_on_x_for_step(self.lambda_)}")
            print(f"Value in zero {self.super_upper_bound_independent_on_x_for_step(0)}")

        if self.step == 'Armijo_Kamalov':
            self.lambda_ = self.sigma_0
            while self.upper_bound_for_step(self.lambda_) > 0:
                self.lambda_ *= self.beta_0

        # Here we get new shifts.
        delta_real, delta_complex = \
            self.coefficients_of_exponents_and_expanded_and_shifts_of_exponents_obtained_from_lambda(self.lambda_)[4:6]
        self.exponents['real'] += delta_real
        self.exponents['complex'] = np.array([complex_exp + delta_complex_exp for complex_exp, delta_complex_exp in
                                              zip(self.exponents['complex'], delta_complex)])

        '''
        # If some powers of exponents are close, we change one of them to x * e^{alpha x}.
        self.exponents['real'] = np.round(self.exponents['real'], self.round_exp)
        self.exponents['complex'] = np.round(self.exponents['complex'],  self.round_exp)
        self.exponents['real'] = np.unique(self.exponents['real'])
        self.exponents['complex'] = np.unique(self.exponents['complex'])
        '''

    def print_in_console(self):
        print(f"Coefficients of real exponents in the polynomial of best approximation "
              f"{self.coefficients_of_real_exponents};\n"
              f"Coefficients of complex exponents in the polynomial of best approximation "
              f"{self.coefficients_of_complex_exponents};\n"
              f"Coefficients of real exponents in the polynomial of best approximation by expanded system "
              f"{self.coefficients_of_real_exp_expanded_system};\n"
              f"Coefficients of complex exponents in the polynomial of best approximation by expanded system "
              f"{self.coefficients_of_complex_exp_expanded_system};\n"
              f"Coefficients of real exponents mult. by x in the polynomial of best approximation by expanded system "
              f"{self.coefficients_of_real_exp_mult_by_x_expanded_system};\n"
              f"Coefficients of complex exponents mult. by x in the polynomial of best approximation by expanded system "
              f"{self.coefficients_of_complex_exp_mult_by_x_expanded_system};\n"
              f"Difference of norm f and first approximation {self.norm_first_system};\n"
              f"Current powers of exponents: {self.exponents};\n"
              f"Difference of norm f and second approximation {self.norm_second_system}.\n")

    def main_body(self):
        if self.loop == "while":
            while self.norm_first_system - self.norm_second_system > self.eps_norm_difference:
                self.main_body_for_loop_for_or_while()
                if self.parent_folder == "local_algorithm":
                    self.print_in_console()

        if self.loop == "for":
            for _ in range(self.number_of_iter):
                self.main_body_for_loop_for_or_while()
                if self.parent_folder == "local_algorithm":
                    self.print_in_console()

        # Here we define the coefficients of the best polynomial.
        nested_list_of_complex_exponents = [self.generate_list_of_complex_exponents(z) for z in
                                            self.exponents['complex']]
        list_of_exponents = [self.generate_list_of_real_exponents(z) for z in self.exponents['real']] + \
                            [exp for sublist in nested_list_of_complex_exponents for exp in sublist]
        self.modified_Remez_coefficients_of_exponents = ModifiedRemezAlgorithmFightWithDegeneracyMinimization(
            self.f_0, list_of_exponents, self.parent_folder, left=self.left, right=self.right)
        self.modified_Remez_coefficients_of_exponents.main_body()
        self.coefficients_first_system = self.modified_Remez_coefficients_of_exponents.coefficients_of_the_best_polynom
        self.coefficients_of_real_exponents = \
            self.modified_Remez_coefficients_of_exponents.coefficients_of_the_best_polynom[
            :len(self.exponents['real'])]
        self.coefficients_of_complex_exponents = \
            self.modified_Remez_coefficients_of_exponents.coefficients_of_the_best_polynom[
            len(self.exponents['real']):]
        self.norm_first_system = self.modified_Remez_coefficients_of_exponents.upper_bound
        self.norm_differences_first_system = np.append(self.norm_differences_first_system, self.norm_first_system)
        self.alternance_points = self.modified_Remez_coefficients_of_exponents.points

        # Here we define the coefficients of the best polynomial by expanded system.
        nested_list_of_complex_exp_mult_by_x = [self.generate_list_of_complex_exponents_multiplied_by_x(z)
                                                for z in self.exponents['complex']]
        functions_of_expanded_system = [self.generate_list_of_real_exponents(z) for z in self.exponents['real']] + \
                                       [self.generate_list_of_real_exponents_multiplied_by_x(z)
                                        for z in self.exponents['real']] + \
                                       [exp for sublist in nested_list_of_complex_exponents for exp in sublist] + \
                                       [exp for sublist in nested_list_of_complex_exp_mult_by_x for exp in sublist]
        self.modified_Remez_exponents = ModifiedRemezAlgorithmFightWithDegeneracyMinimization(
            self.f_0, functions_of_expanded_system, self.parent_folder,
            left=self.left, right=self.right)
        self.modified_Remez_exponents.main_body()
        self.coefficients_first_system = self.modified_Remez_coefficients_of_exponents.coefficients_of_the_best_polynom

        self.coefficients_of_real_exp_expanded_system = \
            self.modified_Remez_exponents.coefficients_of_the_best_polynom[:len(self.exponents['real'])]
        self.coefficients_of_real_exp_mult_by_x_expanded_system = \
            self.modified_Remez_exponents.coefficients_of_the_best_polynom[len(self.exponents['real']):
                                                                           2 * len(self.exponents['real']):]
        self.coefficients_of_complex_exp_expanded_system = \
            self.modified_Remez_exponents.coefficients_of_the_best_polynom[2 * len(self.exponents['real']):
                                                                           2 * len(self.exponents['real']) +
                                                                           2 * len(self.exponents['complex'])]
        self.coefficients_of_complex_exp_mult_by_x_expanded_system = \
            self.modified_Remez_exponents.coefficients_of_the_best_polynom[2 * len(self.exponents['real']) +
                                                                           2 * len(self.exponents['complex']):]
        self.norm_second_system = self.modified_Remez_exponents.upper_bound
        self.norm_differences_expanded_system = np.append(self.norm_differences_expanded_system,
                                                          self.norm_second_system)
        self.alternance_points_expanded = self.modified_Remez_exponents.points

        # Let us write results into file.
        if self.parent_folder == "local_algorithm":
            # Let us write the results into the file.
            Path(Path.cwd().parent.parent.parent / "results" / "exponential_analysis" / "local_algorithm" /
                 self.dir_with_results).mkdir(parents=True, exist_ok=True)
            results = Path.cwd().parent.parent.parent / "results" / "exponential_analysis" / "local_algorithm" / \
                      self.dir_with_results / self.file_with_results
            target = results.open("w", encoding="UTF-8")
            self.initial_exponents = {key: value.tolist() if not isinstance(value, list) else value
                                      for (key, value) in self.exponents.items()}
            dict_with_results = {'initial_powers_of_exponents': self.initial_exponents,
                                 'norm_f_minus_h(0,c)': list(self.norm_differences_first_system),
                                 'difference_norm_given_system_minus_expanded':
                                     list(self.norm_differences_first_system - self.norm_differences_expanded_system),
                                 'coefficients_of_the_best_polynom_real_exp': list(self.coefficients_of_real_exponents),
                                 'coefficients_of_the_best_polynom_complex_exp':
                                     list(self.coefficients_of_complex_exponents),
                                 'powers of exponents': {key: value.tolist()
                                                         for (key, value) in self.exponents.items()}}
            json.dump(dict_with_results, target, indent=4)

            # Figure with the rate of convergence of residuals.
            temp_address = results.stem + '_convergence_of_residuals'
            norm_f_minus_h_minus_h_opt = (self.norm_differences_first_system -
                                          self.norm_differences_first_system[-1])[:-1]
            norm_f_minus_h_minus_h_opt = [norm_f for norm_f in norm_f_minus_h_minus_h_opt if norm_f > 0]
            fig = plt.figure()
            a = fig.add_axes([0, 0, 1, 1])

            a.set_title("Convergence Rate")
            a.set_xlabel("Iteration number")
            a.set_ylabel(r"$\ln(h(c,0)-h^{*})$")
            a.grid(color='b', ls='-.', lw=0.5)
            a.plot(np.arange(1, len(np.log(norm_f_minus_h_minus_h_opt)) + 1),
                   np.log(norm_f_minus_h_minus_h_opt), 'ro-')
            a.set_xticks(np.arange(1, len(np.log(norm_f_minus_h_minus_h_opt)) + 1, 2))
            fig.savefig(results.parent / temp_address, bbox_inches='tight')

            # Figure with the rate of convergence.
            temp_address = results.stem + '_convergence'
            norm_f_minus_h = self.norm_differences_first_system

            fig = plt.figure()
            a = fig.add_axes([0, 0, 1, 1])

            a.set_title("Convergence Rate")
            a.set_xlabel("Iteration number")
            a.set_ylabel(r"$\ln(h(c,0))-h(a,b)$")
            a.grid(color='b', ls='-.', lw=0.5)
            a.plot(np.arange(1, len(np.log(norm_f_minus_h)) + 1), np.log(norm_f_minus_h), 'ro-')
            a.set_xticks(np.arange(1, len(np.log(norm_f_minus_h)) + 1, 2))
            fig.savefig(results.parent / temp_address, bbox_inches='tight')

            # Figure with the alternance for final polynomial of the best approximation.
            temp_address = results.stem + '_alternance.png'
            save_path = results.parent / temp_address
            fig = plt.figure()
            a = fig.add_axes([0, 0, 1, 1])
            a.set_title(r'Difference of Objective Function $f_0$ and Polynomial $p$')
            a.set_xlabel('Points')
            a.set_ylabel(r'$f_0-p$')
            a.grid(color='b', ls='-.', lw=0.5)
            points = np.arange(self.left, self.right, 0.0001)
            nested_list_of_complex_exp = [self.generate_list_of_complex_exponents(z) for z in self.exponents['complex']]
            functions = [self.generate_list_of_real_exponents(z) for z in self.exponents['real']] + \
                        [exp for sublist in nested_list_of_complex_exp for exp in sublist]
            values = np.array([lambda x=y: self.f_0(x) - self.polynom(functions, self.coefficients_first_system, x)
                               for y in points])
            values_in_alternance_points = np.array([lambda x=y: self.f_0(x) - self.polynom(functions,
                                                                                           self.
                                                                                           coefficients_first_system,
                                                                                           x)
                                                    for y in self.alternance_points])
            a.plot(points, [value() for value in values], 'r')
            a.scatter(self.alternance_points, [value() for value in values_in_alternance_points], color='black')
            a.set_xticks(np.arange(self.left, self.right + 0.01, (self.right - self.left) / 10))
            a.vlines(x=self.alternance_points, ymin=0, ymax=[value() for value in values_in_alternance_points],
                     color='green',
                     ls='--')
            a.hlines(y=0, xmin=self.left, xmax=self.right, color='blue')
            save_path.parent.mkdir(parents=True, exist_ok=True)
            fig.savefig(save_path, bbox_inches='tight')

            # Figure with the function f_0 and polynomial of the best approximation.
            temp_address = results.stem + '_function_and_polynomial'
            fig = plt.figure()
            a = fig.add_axes([0, 0, 1, 1])
            a.set_title(r'Objective Function $f_0$ and Polynomial of Best Approximation $p$')
            a.set_xlabel('Points')
            a.set_ylabel(r'$f_0, p$')
            a.grid(color='b', ls='-.', lw=0.5)
            points = np.arange(self.left, self.right + 0.01, 0.0001)
            values_of_f_0 = np.array([lambda x=y: self.f_0(x) for y in points])
            nested_list_of_complex_exp = [self.generate_list_of_complex_exponents(z) for z in self.exponents['complex']]
            functions = [self.generate_list_of_real_exponents(z) for z in self.exponents['real']] + \
                        [exp for sublist in nested_list_of_complex_exp for exp in sublist]
            values_of_polynomial = np.array([lambda x=y: self.polynom(functions, self.coefficients_first_system, x)
                                             for y in points])
            a.plot(points, [value() for value in values_of_f_0], 'r', label=r'$f_0$')
            a.plot(points, [value() for value in values_of_polynomial], 'b', linewidth=2.5, label=r'$p$')
            a.set_xticks(np.arange(self.left, self.right + 0.01, (self.right - self.left) / 10))
            a.legend(loc='upper right')
            # plt.show()
            fig.savefig(results.parent / temp_address, bbox_inches='tight')

            return results
