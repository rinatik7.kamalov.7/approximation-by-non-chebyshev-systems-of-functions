import math as m
import numpy as np
from numpy.linalg import lstsq, eig
from pathlib import Path
from scipy.optimize import dual_annealing, linprog


class CutTailPoint:
    def __init__(self, functions, file_with_results, right=10, abs_tol_eigen_value=1e-12, abs_tol_lin_prog=1e-4,
                 epsilon_from_right=1e-4, abs_tol_lin_system=1e-6, epsilon_lower_upper=1e-6, epsilon_cut_tail=1e-4,
                 epsilon_dichotomy=1e-6, epsilon_degen_polynom=1e-6, mult_degen=10):

        self.dir_with_results = file_with_results
        self.file_with_results = file_with_results + 'json'

        self.upper_cut_tail = right
        self.lower_cut_tail = 0

        self.functions = functions

        self.epsilon_from_right = epsilon_from_right
        self.abs_tol_eigen_value = abs_tol_eigen_value
        self.abs_tol_lin_system = abs_tol_lin_system
        self.abs_tol_lin_prog = abs_tol_lin_prog
        self.epsilon_lower_upper = epsilon_lower_upper
        self.epsilon_cut_tail = epsilon_cut_tail
        self.mult_degen = mult_degen
        self.epsilon_dichotomy = epsilon_dichotomy
        self.epsilon_degen_polynom = epsilon_degen_polynom

        '''These parameters are defined in the inner algorithm.'''
        self.number_of_iterations = None
        self.lower_bound = None
        self.upper_bound = None
        self.upper_bound_minus_lower_bound = np.array([])
        self.coefficients_of_the_best_polynom = None
        self.cut_tail_point = None
        self.points = None  # We bring one point less than in approximation problem.
        self.points_with_cut_tail_point = None
        self.values_of_functions_in_cut_tail_point = None
        self.values_of_functions_on_grid = None  # Rows are values for fixed point.
        self.projection_values_of_function_on_greed = None
        self.matrix_a = None  # Rows of matrix are vectors a_i.
        self.matrix_a_projection_on_orthogonal = None
        self.sigma = None
        # self.values_of_f_0_on_grid = np.array([f_0(point) for point in self.points])
        self.extremal_point = None
        self.extremal_value = None
        self.a_0 = None
        self.a_0_projection = None
        self.index_for_a_0 = None
        self.sigma_0 = None
        self.exist_polynom = None

    '''We project vector_a onto orthogonal complement of vector_b.'''

    @staticmethod
    def projection_vector_onto_subspace_orthogonal_to_given_vector(vector_a, vector_b):
        projection = vector_a - (np.dot(vector_a, vector_b) / np.dot(vector_b, vector_b)) * vector_b
        return projection

    def solve_linear_system(self):
        matrix_lin_system = np.append(self.matrix_a, np.array([self.values_of_functions_in_cut_tail_point]), axis=0)
        last_column_of_matrix = np.append(np.full(np.shape(matrix_lin_system)[1], -1), 0)
        matrix_lin_system = np.c_[matrix_lin_system, last_column_of_matrix]
        vector_b = np.append(np.zeros(np.shape(matrix_lin_system)[0] - 1), 1)
        p_and_d_sol = lstsq(matrix_lin_system, vector_b, rcond=None)[0]
        if not np.allclose(np.dot(matrix_lin_system, p_and_d_sol), vector_b, atol=self.abs_tol_lin_system):
            raise Exception("The solution of linear system is not accurate.")
        return p_and_d_sol

    def polynom(self, x):
        return np.dot([f(x) for f in self.functions], self.coefficients_of_the_best_polynom)

    def the_worst_point_for_the_best_approximation(self):
        self.extremal_point, self.extremal_value = (dual_annealing(lambda x: -(self.polynom(x)) ** 2,
                                                                   [(0, self.cut_tail_point)]).x,
                                                    m.sqrt(
                                                        - dual_annealing(lambda x: -(self.polynom(x)) ** 2,
                                                                         [(0, self.cut_tail_point)]).fun))
        self.sigma_0 = int(np.sign(self.polynom(self.extremal_point)))
        self.a_0 = self.sigma_0 * np.array([f(self.extremal_point) for f in self.functions], dtype=float)
        self.a_0_projection = self.projection_vector_onto_subspace_orthogonal_to_given_vector \
            (self.a_0, self.values_of_functions_in_cut_tail_point)

    @staticmethod
    def minus_intersection_ray_with_face(vector, vectors):
        matrix_equalities = np.c_[vectors.T, vector]
        last_raw_in_matrix = np.array([np.append(np.ones(np.shape(matrix_equalities)[1] - 1), 0)])
        matrix_equalities = np.append(matrix_equalities, last_raw_in_matrix, axis=0)
        vector_equalities = np.append(np.zeros(np.shape(matrix_equalities)[0] - 1), 1)
        objective_vector = np.append(np.zeros(np.shape(matrix_equalities)[0] - 1), -1)
        res = linprog(objective_vector, A_eq=matrix_equalities, b_eq=vector_equalities, method='highs-ds').x
        # print(f"Лин. прог. {linprog(objective_vector, A_eq=matrix_equalities, b_eq=vector_equalities, method='highs-ds')}")
        return res

    @staticmethod
    def minus_lies_in_conic_hull_or_not(vector, vectors):
        matrix_equalities = vectors.T
        vector_equalities = - vector
        objective_vector = np.zeros(np.shape(matrix_equalities)[1])
        res = linprog(objective_vector, A_eq=matrix_equalities, b_eq=vector_equalities, method='highs-ds')
        return res

    def zero_in_convex_hull(self):
        res = self.minus_intersection_ray_with_face(self.a_0_projection, self.matrix_a_projection_on_orthogonal)
        index_for_extra_a_i_new = np.array([])
        for index, vector in enumerate(self.matrix_a_projection_on_orthogonal):
            res_1 = \
                self.minus_lies_in_conic_hull_or_not(self.a_0_projection,
                                                     np.array(list(self.matrix_a_projection_on_orthogonal[:index]) +
                                                              list(self.matrix_a_projection_on_orthogonal[
                                                                   index + 1:]))).success
            if res_1:
                index_for_extra_a_i_new = np.append(index_for_extra_a_i_new, index)
        # scalar_products = [np.dot(-self.a_0_projection, a_i) for a_i in self.matrix_a_projection_on_orthogonal]
        # index_for_extra_a_i, = np.where(res[:-1] <= 1e-12)  # The last coefficient is not related to a_i.
        index_for_extra_a_i, = np.where(res[:-1] == 0)
        index_for_extra_a_i = np.random.choice(index_for_extra_a_i, size=1)
        # index_for_extra_a_i_new = scalar_products.index(min(scalar_products))
        # assert index_for_extra_a_i == index_for_extra_a_i_new, 'Индексы для лишнего a_i не совпали.'
        # index_for_extra_a_i = min(index_for_extra_a_i)
        # assert len(index_for_extra_a_i), "There is no raw a_i in matrix_a such that matrix_a with raw a_0 " \
        #                                 "instead of a_i involves point zero."
        # print(f"Матрица проекций {self.matrix_a_projection_on_orthogonal}.")
        self.matrix_a_projection_on_orthogonal[index_for_extra_a_i] = self.a_0_projection
        self.matrix_a[index_for_extra_a_i] = self.a_0

    def print_in_console(self):
        print(f'Lower bound: {self.lower_bound}; \n Upper bound: {self.upper_bound};\n'
              f'Coefficients of the best polynom: {self.coefficients_of_the_best_polynom};\n'
              f' Number of iterations: {self.number_of_iterations}.\n\n')

    def main_body(self):
        # Part related to Cut_Tail_Point.
        while self.upper_cut_tail - self.lower_cut_tail > self.epsilon_cut_tail:
            self.cut_tail_point = (self.lower_cut_tail + self.upper_cut_tail) / 2
            print(f"Cut tail point {self.cut_tail_point}")

            self.number_of_iterations = 0
            self.lower_bound = 0
            self.upper_bound = np.inf

            self.points = np.linspace(0, 0.9 * self.cut_tail_point, len(self.functions), endpoint=False)
            self.points_with_cut_tail_point = np.append(self.points, self.cut_tail_point)
            self.values_of_functions_on_grid = np.array([[f(point) for f in self.functions]
                                                         for point in self.points])  # Rows are the values for fixed point.
            self.values_of_functions_in_cut_tail_point = np.array([f(self.cut_tail_point) for f in self.functions])
            self.projection_values_of_function_on_greed = \
                np.array([self.projection_vector_onto_subspace_orthogonal_to_given_vector(
                    v_i, self.values_of_functions_in_cut_tail_point) for v_i in self.values_of_functions_on_grid])

            # Initialization of inner algorithm.
            eig_values, eig_vectors = eig(self.projection_values_of_function_on_greed.T)
            dict_with_indexes_of_real_eigenvalues = {index: abs(eigvalue) for index, eigvalue in enumerate(eig_values)
                                                     if eigvalue.imag == 0}
            index_for_eigenvalue_zero = min(dict_with_indexes_of_real_eigenvalues.items(), key=lambda x: x[1])[0]
            solution = eig_vectors[:, index_for_eigenvalue_zero].real
            self.sigma = np.sign(solution)
            if not np.all(self.sigma):
                raise Exception("Some components of vector sigma are equal to zero.")
            self.matrix_a = np.array([sigma_i * values_of_functions_on_grid
                                      for sigma_i, values_of_functions_on_grid
                                      in
                                      zip(self.sigma, self.values_of_functions_on_grid)])
            self.matrix_a_projection_on_orthogonal = np.array([sigma_i * projection_values_of_function_on_greed
                                                               for sigma_i, projection_values_of_function_on_greed
                                                               in
                                                               zip(self.sigma,
                                                                   self.projection_values_of_function_on_greed)])

            # Iterations of inner algorithm.
            self.exist_polynom = False
            while self.upper_bound - self.lower_bound > self.epsilon_lower_upper:
                # while self.number_of_iterations < 10:
                self.number_of_iterations += 1
                assert self.lower_bound <= self.upper_bound, "The lower bound is more than the upper bound."
                p_and_d_sol = self.solve_linear_system()
                self.coefficients_of_the_best_polynom = p_and_d_sol[:-1]
                self.lower_bound = max(p_and_d_sol[-1], self.lower_bound)
                assert self.lower_bound >= 0, "The lower bound is less than zero."
                self.the_worst_point_for_the_best_approximation()
                '''In this case T_cut > T.'''
                if self.extremal_point == self.cut_tail_point:
                    self.lower_cut_tail = self.cut_tail_point
                    self.upper_cut_tail = self.upper_cut_tail
                    self.exist_polynom = True
                    break
                self.zero_in_convex_hull()
                self.upper_bound = min(self.extremal_value, self.upper_bound)
                assert self.upper_bound >= 0, "The upper bound is less than zero."

                self.upper_bound_minus_lower_bound = np.append(self.upper_bound_minus_lower_bound,
                                                               self.upper_bound - self.lower_bound)

            # self.print_in_console()
            if not self.exist_polynom:
                if self.upper_bound - self.epsilon_lower_upper > 1:
                    self.lower_cut_tail = self.lower_cut_tail
                    self.upper_cut_tail = self.cut_tail_point
                else:
                    self.lower_cut_tail = self.cut_tail_point
                    self.upper_cut_tail = self.upper_cut_tail

        return self.cut_tail_point
