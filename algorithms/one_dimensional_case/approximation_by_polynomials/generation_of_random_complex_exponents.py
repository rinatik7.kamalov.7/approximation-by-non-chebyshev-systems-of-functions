import math as m
import numpy as np


class GenerationOfRandomComplexFunctions:
    def __init__(self, number_of_complex_exponents):
        self.number_of_complex_exponents = number_of_complex_exponents
        self.complex_components = None
        self.approximating_functions = np.array([])

    def generation_of_exponents(self):
        self.complex_components = np.sqrt(np.random.uniform(0, 1, self.number_of_complex_exponents)) * \
                                  np.exp(1.j * np.random.uniform(0, 2 * np.pi, self.number_of_complex_exponents))

    @staticmethod
    def generation_of_pair_of_functions(complex_number):
        def f_1(x):
            return m.exp(complex_number.real * x) * m.sin(complex_number.imag * x)

        def f_2(x):
            return m.exp(complex_number.real * x) * m.cos(complex_number.imag * x)

        return [f_1, f_2]

    def generation_of_all_functions(self):
        for number in self.complex_components:
            self.approximating_functions = np.append(self.approximating_functions,
                                                     self.generation_of_pair_of_functions(number))

    def main_body(self):
        self.generation_of_exponents()
        self.generation_of_all_functions()
