import json
import math as m
import numpy as np
import warnings
import matplotlib.pyplot as plt
from numpy.linalg import lstsq, matrix_rank
from pathlib import Path
from scipy.optimize import dual_annealing, linprog


class ModifiedRemezAlgorithm:
    def __init__(self, f_0, functions, parent_folder, file_with_results, initial_points=None, left=0, right=1,
                 abs_tol_lin_system=1e-6, epsilon_lower_upper=1e-6):
        warnings.filterwarnings("ignore", category=np.VisibleDeprecationWarning)
        self.parent_folder = parent_folder
        self.dir_with_results = file_with_results
        self.file_with_results = file_with_results + '.json'

        self.f_0 = f_0
        self.left = left
        self.right = right
        self.points = np.linspace(left, right, len(functions) + 1) if not initial_points else np.array(initial_points)
        self.functions = functions
        self.abs_tol_lin_system = abs_tol_lin_system
        self.epsilon_lower_upper = epsilon_lower_upper

        self.number_of_iterations = 0
        self.lower_bound = 0
        self.upper_bound = np.inf
        self.upper_bound_minus_lower_bound = np.array([])
        self.coefficients_of_the_best_polynom = None
        self.values_of_functions_on_grid = np.array([[f(point) for f in self.functions]
                                                     for point in self.points])  # Rows are values for fixed point.
        self.extremal_point = None
        self.extremal_value = None
        self.matrix_a = None  # Rows of the matrix are vectors a_i.
        self.a_0 = None
        self.sigma_0 = None
        self.sigma = None
        self.values_of_f_0_on_grid = np.array([f_0(point) for point in self.points])

    def solve_linear_system(self):
        matrix_lin_system = np.c_[self.matrix_a, np.full(np.shape(self.matrix_a)[0], -1)]
        assert np.shape(self.sigma) == np.shape(self.values_of_f_0_on_grid), "Shapes of sigma and values of f_0 on " \
                                                                             "the grid do not match."
        vector_b = np.multiply(self.sigma, self.values_of_f_0_on_grid)
        p_and_d_sol = lstsq(matrix_lin_system, vector_b, rcond=None)[0]
        if not np.allclose(np.dot(matrix_lin_system, p_and_d_sol), vector_b, atol=self.abs_tol_lin_system):
            raise Exception("The solution of the linear system for lower bound is not accurate.")
        return p_and_d_sol

    def polynom(self, x):
        return np.dot([f(x) for f in self.functions], self.coefficients_of_the_best_polynom)

    def the_worst_point_for_the_best_approximation(self):
        self.extremal_point, self.extremal_value = (dual_annealing(lambda x: -(self.polynom(x) - self.f_0(x)) ** 2,
                                                                   [(self.left, self.right)]).x,
                                                    m.sqrt(
                                                        - dual_annealing(lambda x: -(self.polynom(x) - self.f_0(x)) ** 2,
                                                                         [(self.left, self.right)]).fun))
        self.sigma_0 = int(np.sign(self.polynom(self.extremal_point) - self.f_0(self.extremal_point)))
        self.a_0 = self.sigma_0 * np.array([f(self.extremal_point) for f in self.functions], dtype=float)

    def minus_intersection_ray_with_face(self, vector, vectors):
        matrix_equalities = np.c_[vectors.T, vector]
        last_raw_in_matrix = np.array([np.append(np.ones(np.shape(matrix_equalities)[1] - 1), 0)])
        matrix_equalities = np.append(matrix_equalities, last_raw_in_matrix, axis=0)
        vector_equalities = np.append(np.zeros(np.shape(matrix_equalities)[0] - 1), 1)
        objective_vector = np.append(np.zeros(np.shape(matrix_equalities)[1] - 1), -1)
        res = linprog(objective_vector, A_eq=matrix_equalities, b_eq=vector_equalities, method='highs-ds')
        if not res.success:
            raise Exception(f"The vector a_0 does not lie in the convex hull of previous vectors. The matrix a is \n "
                            f"{self.matrix_a}. The vector a_0 is {self.a_0}.")
        return res.x

    def zero_in_convex_hull(self):
        res = self.minus_intersection_ray_with_face(self.a_0, self.matrix_a)
        index_for_extra_a_i, = np.where(res[:-1] == 0)
        index_for_extra_a_i = np.random.choice(index_for_extra_a_i, size=1)

        self.points[index_for_extra_a_i] = self.extremal_point
        self.matrix_a[index_for_extra_a_i] = self.a_0
        self.sigma[index_for_extra_a_i] = self.sigma_0
        self.values_of_functions_on_grid[index_for_extra_a_i] = np.array([f(self.extremal_point) for f in self.functions])
        self.values_of_f_0_on_grid[index_for_extra_a_i] = self.f_0(self.extremal_point)

    def print_in_console(self):
        print(f'Lower bound: {self.lower_bound}; \n Upper bound: {self.upper_bound};\n'
              f'Coefficients of the best polynom: {self.coefficients_of_the_best_polynom};\n'
              f'Alternance points: {self.points};\n\n')

    def main_body(self):
        # Algorithm initialization.
        if matrix_rank(self.values_of_functions_on_grid) != \
                np.shape(self.values_of_functions_on_grid)[1]:
            raise Exception("The original matrix has an incomplete rank.")

        constraint = np.random.rand(np.shape(self.values_of_functions_on_grid.T)[1])
        constraint /= np.sum(constraint)
        values_of_func_on_grid_and_linear_constraint = np.append(self.values_of_functions_on_grid.T,
                                                                 [constraint], axis=0)
        '''
        values_of_func_on_grid_and_linear_constraint = \
            np.append(self.values_of_functions_on_grid.T,
                      [np.ones(np.shape(self.values_of_functions_on_grid)[0])], axis=0)
        '''

        right_part_without_linear_constraint = [0] * (np.shape(self.values_of_functions_on_grid)[1])
        right_part_with_linear_constraint = np.append(right_part_without_linear_constraint, 1)
        solution = lstsq(values_of_func_on_grid_and_linear_constraint, right_part_with_linear_constraint, rcond=None)[0]
        if not np.allclose(np.dot(values_of_func_on_grid_and_linear_constraint, solution),
                           right_part_with_linear_constraint, atol=self.abs_tol_lin_system):
            raise Exception("The solution of the linear system for initialization is not accurate.")
        self.sigma = np.sign(solution)
        if not np.all(self.sigma):
            raise Exception("Some components of vector sigma are equal to zero.")
        self.matrix_a = np.array([sigma_i * values_of_functions_on_grid_column
                                  for sigma_i, values_of_functions_on_grid_column
                                  in zip(self.sigma, self.values_of_functions_on_grid)])

        # Iterations of the algorithm.
        while self.upper_bound - self.lower_bound > self.epsilon_lower_upper:
            self.number_of_iterations += 1
            p_and_d_sol = self.solve_linear_system()
            self.coefficients_of_the_best_polynom = p_and_d_sol[:-1]
            self.lower_bound = max(p_and_d_sol[-1], self.lower_bound)
            assert self.lower_bound >= 0, "The lower bound is less than zero."
            self.the_worst_point_for_the_best_approximation()
            self.zero_in_convex_hull()
            self.upper_bound = min(self.extremal_value, self.upper_bound)
            assert self.upper_bound >= 0, "The upper bound is less than zero."

            if self.upper_bound - self.lower_bound >= 0:
                self.upper_bound_minus_lower_bound = np.append(self.upper_bound_minus_lower_bound,
                                                               self.upper_bound - self.lower_bound)

            if self.parent_folder == "modified_Remez_algorithm" or self.parent_folder == \
                    "modified_Remez_algorithm_fight_minimization":
                self.print_in_console()

        if self.parent_folder == "modified_Remez_algorithm" or self.parent_folder == \
                "modified_Remez_fight_minimization":
            # Let us write the results into the file.
            Path(Path.cwd().parent.parent / "results" / "modified_Remez_algorithm" /
                 self.dir_with_results).mkdir(parents=True, exist_ok=True)
            results = Path.cwd().parent.parent / "results" / "modified_Remez_algorithm" / \
                      self.dir_with_results / self.file_with_results
            target = results.open("w", encoding="UTF-8")
            dict_with_results = {'coefficients_of_the_best_polynom': list(self.coefficients_of_the_best_polynom),
                                 'alternance_points': list(self.points),
                                 'upper_bound_minus_lower_bound_at_each_step': list(self.upper_bound_minus_lower_bound)}
            json.dump(dict_with_results, target, indent=4)

            # Figure with the function f_0 and polynomial of the best approximation.
            temp_address = results.stem + '_function_and_polynomial'
            fig = plt.figure()
            a = fig.add_axes([0, 0, 1, 1])
            a.set_title(r'Objective Function $f_0$ and Polynomial of Best Approximation $p$')
            a.set_xlabel('Points')
            a.set_ylabel(r'$f_0, p$')
            a.grid(color='b', ls='-.', lw=0.5)
            points = np.arange(self.left, self.right + 0.01, 0.01)
            values_of_f_0 = np.array([lambda x=y: self.f_0(x) for y in points])
            values_of_polynomial = np.array([lambda x=y: self.polynom(x) for y in points])

            a.plot(points, [value() for value in values_of_f_0], 'r', label=r'$f_0$')
            a.plot(points, [value() for value in values_of_polynomial], 'b', label=r'$p$')
            a.set_xticks(np.arange(self.left, self.right + 0.01, (self.right - self.left) / 10))
            a.legend(loc='upper right')
            fig.savefig(results.parent / temp_address, bbox_inches='tight')

            # Figure with the alternance for final polynomial of best approximation.
            temp_address = results.stem + '_alternance'
            fig = plt.figure()
            a = fig.add_axes([0, 0, 1, 1])
            a.set_title(r'Difference of Objective Function $f_0$ and Polynomial $p$')
            a.set_xlabel('Points')
            a.set_ylabel(r'$f_0-p$')
            a.grid(color='b', ls='-.', lw=0.5)
            points = np.arange(self.left, self.right + 0.01, 0.01)
            values = np.array([lambda x=y: self.f_0(x) - self.polynom(x) for y in points])
            a.plot(points, [value() for value in values], 'r')
            values_in_alternance_points = np.array([lambda x=y: self.f_0(x) - self.polynom(x) for y in self.points])
            a.scatter(self.points, [value() for value in values_in_alternance_points], color='black')
            a.set_xticks(np.arange(self.left, self.right + 0.01, (self.right - self.left) / 10))
            a.vlines(x=self.points, ymin=0, ymax=[value() for value in values_in_alternance_points],
                     color='green',
                     ls='--')
            a.hlines(y=0, xmin=self.left, xmax=self.right, color='blue')
            fig.savefig(results.parent / temp_address, bbox_inches='tight')

            # Figure with the rate of convergence.
            temp_address = results.stem + '_convergence'
            upper_minus_lower = dict_with_results['upper_bound_minus_lower_bound_at_each_step']

            fig = plt.figure()
            a = fig.add_axes([0, 0, 1, 1])

            a.set_title("Convergence Rate")
            a.set_xlabel("Iteration number")
            a.set_ylabel(r"$ln(ub-lb)$")
            a.grid(color='b', ls='-.', lw=0.5)
            a.plot(np.arange(1, len(np.log(upper_minus_lower)) + 1), np.log(upper_minus_lower), 'ro-')
            a.set_xticks(np.arange(1, len(np.log(upper_minus_lower)) + 1, 2))

            fig.savefig(results.parent / temp_address, bbox_inches='tight')

            return results
