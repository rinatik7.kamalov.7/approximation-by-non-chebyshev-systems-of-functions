import json
import math as m
import numpy as np
from numpy.linalg import lstsq, matrix_rank
from pathlib import Path
from scipy.optimize import dual_annealing


class LeastSquareMethod:
    def __init__(self, f_0, functions, number_of_points_on_segment, parent_folder, file_with_results,
                 left=0, right=1, abs_tol_lin_system=1e-6):
        self.parent_folder = parent_folder
        self.dir_with_results = file_with_results
        self.file_with_results = file_with_results + '.json'

        self.f_0 = f_0
        self.left = left
        self.right = right

        self.functions = functions
        self.points_on_segment = np.linspace(left, right, number_of_points_on_segment)
        self.abs_tol_lin_system = abs_tol_lin_system

        self.values_of_functions_on_grid = np.array([[f(point) for f in self.functions]
                                                     for point in self.points_on_segment])
        self.values_of_f_0_on_grid = np.array([f_0(point) for point in self.points_on_segment])
        self.coefficients_of_the_best_polynom = None

    def least_square(self):
        p_sol = lstsq(self.values_of_functions_on_grid, self.values_of_f_0_on_grid, rcond=None)[0]
        return p_sol

    def norm_of_the_best_approximation(self):
        def polynom(x):
            return np.dot([f(x) for f in self.functions], self.coefficients_of_the_best_polynom)

        extremal_point, extremal_value = (dual_annealing(lambda x: -(polynom(x) - self.f_0(x)) ** 2,
                                                         [(self.left, self.right)]).x,
                                          m.sqrt(- dual_annealing(lambda x: -(polynom(x) - self.f_0(x)) ** 2,
                                                                  [(self.left, self.right)]).fun))
        return extremal_point, extremal_value

    def main_body(self):
        self.coefficients_of_the_best_polynom = self.least_square()
        extremal_value = self.norm_of_the_best_approximation()[1]

        return extremal_value





