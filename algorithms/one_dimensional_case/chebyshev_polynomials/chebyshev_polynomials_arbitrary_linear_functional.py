import json
import math as m
import matplotlib.pyplot as plt
import numpy as np
import warnings
from numpy.linalg import lstsq, eig
from pathlib import Path
from scipy.optimize import dual_annealing, linprog, root


class ChebyshevPolynomials:
    def __init__(self, functions, linear_functional, file_with_results, const_lin_bound=1, left=-1, right=1,
                 abs_tol_eigen_value=1e-6, abs_tol_lin_system=1e-6, epsilon_lower_upper=1e-6, epsilon_dichotomy=1e-6,
                 epsilon_degen_polynom=1e-6, epsilon_root=1e-6, mult_degen=10):
        self.discarded_values_of_functions_on_grid = None
        self.discarded_sigma = None
        self.discarded_a_projection = None
        self.discarded_a = None
        self.discarded_point = None
        self.num_of_degenerate_vertex = None
        self.degen_or_not_degen = None
        self.degen_at_least_once = None
        self.coefficients_of_polynom_for_fight_with_degeneracy = None
        self.values_of_functions_on_grid_without_a_0_and_a_j_projection = None
        self.values_of_functions_on_grid_without_a_0_and_a_j = None
        warnings.filterwarnings("ignore", category=np.VisibleDeprecationWarning)
        self.dir_with_results = file_with_results
        self.file_with_results = file_with_results + '.json'

        self.const_lin_bound = const_lin_bound
        self.left = left
        self.right = right

        self.functions = functions

        # The vector l \in R^n that represents given lin. functional.
        self.linear_functional = np.array(linear_functional)

        self.abs_tol_eigen_value = abs_tol_eigen_value
        self.abs_tol_lin_system = abs_tol_lin_system
        self.epsilon_lower_upper = epsilon_lower_upper
        self.mult_degen = mult_degen
        self.epsilon_dichotomy = epsilon_dichotomy
        self.epsilon_degen_polynom = epsilon_degen_polynom
        self.epsilon_root = epsilon_root

        """These parameters are defined in the inner algorithm."""
        self.number_of_iterations = None
        self.lower_bound = None
        self.upper_bound = None
        self.upper_bound_minus_lower_bound = np.array([])
        self.coefficients_of_the_best_polynom = None
        self.points = None  # We bring one point less than in approximation problem.
        self.values_of_functions_on_grid = None  # Rows are values for fixed point.
        self.projection_values_of_function_on_greed = None
        self.matrix_a = None  # Rows of matrix are vectors a_i.
        self.matrix_a_projection_on_orthogonal = None
        self.sigma = None
        # self.values_of_f_0_on_grid = np.array([f_0(point) for point in self.points])
        self.extremal_point = None
        self.extremal_value = None
        self.a_0 = None
        self.a_0_projection = None
        self.index_for_a_0 = None
        self.sigma_0 = None
        self.roots = np.array([])  # Roots of the polynomial of the best approximation.

    @staticmethod
    def projection_vector_onto_subspace_orthogonal_to_given_vector(vector_a, vector_b):
        """We project vector_a onto orthogonal complement of vector_b."""
        projection = vector_a - (np.dot(vector_a, vector_b) / np.dot(vector_b, vector_b)) * vector_b
        return projection

    def solve_linear_system(self):
        """We solve the linear system (p, a_i) - d = 0; l(p) = (l, p) = const_lin_bound."""
        matrix_lin_system = np.append(self.matrix_a, np.array([self.linear_functional]), axis=0)
        last_column_of_matrix = np.append(np.full(np.shape(matrix_lin_system)[1], - 1), 0)
        matrix_lin_system = np.c_[matrix_lin_system, last_column_of_matrix]
        vector_b = np.append(np.zeros(np.shape(matrix_lin_system)[0] - 1), self.const_lin_bound)
        p_and_d_sol = lstsq(matrix_lin_system, vector_b, rcond=None)[0]
        if not np.allclose(np.dot(matrix_lin_system, p_and_d_sol), vector_b, atol=self.abs_tol_lin_system):
            raise Exception("The solution of linear system is not accurate.")
        return p_and_d_sol

    def polynom(self, x):
        """Polynomial of the best approximation at current step of the algorithm. """
        return np.dot([f(x) for f in self.functions], self.coefficients_of_the_best_polynom)
        # return np.sum([f(x) for f in self.functions]) * np.array(self.coefficients_of_the_best_polynom)

    def the_worst_point_for_the_best_approximation(self):
        """We find ||f-p||_{C[left, right]}."""
        self.extremal_point, self.extremal_value = (dual_annealing(lambda x: -(self.polynom(x)) ** 2,
                                                                   [(self.left, self.right)]).x,
                                                    m.sqrt(
                                                        - dual_annealing(lambda x: -(self.polynom(x)) ** 2,
                                                                         [(self.left, self.right)]).fun))
        self.sigma_0 = int(np.sign(self.polynom(self.extremal_point)))
        self.a_0 = self.sigma_0 * np.array([f(self.extremal_point) for f in self.functions], dtype=float)
        self.a_0_projection = self.projection_vector_onto_subspace_orthogonal_to_given_vector \
            (self.a_0, self.linear_functional)

    @staticmethod
    def minus_intersection_ray_with_face(vector, vectors):
        matrix_equalities = np.c_[vectors.T, vector]
        last_raw_in_matrix = np.array([np.append(np.ones(np.shape(matrix_equalities)[1] - 1), 0)])
        matrix_equalities = np.append(matrix_equalities, last_raw_in_matrix, axis=0)
        vector_equalities = np.append(np.zeros(np.shape(matrix_equalities)[0] - 1), 1)
        objective_vector = np.append(np.zeros(np.shape(matrix_equalities)[0] - 1), -1)
        res = linprog(objective_vector, A_eq=matrix_equalities, b_eq=vector_equalities, method='highs-ds')
        return res.x

    @staticmethod
    def minus_lies_in_conic_hull_or_not(vector, vectors):
        matrix_equalities = vectors.T
        vector_equalities = - vector
        objective_vector = np.zeros(np.shape(matrix_equalities)[1])
        res = linprog(objective_vector, A_eq=matrix_equalities, b_eq=vector_equalities, method='highs-ds')
        return res

    def zero_in_convex_hull(self):
        # print(f"Матрица проекций до проверки {self.matrix_a_projection_on_orthogonal}.")
        res = self.minus_intersection_ray_with_face(self.a_0_projection, self.matrix_a_projection_on_orthogonal)
        '''
        index_for_extra_a_i_new = np.array([])
        for index, vector in enumerate(self.matrix_a_projection_on_orthogonal):
            res_1 = \
                self.minus_lies_in_conic_hull_or_not(self.a_0_projection,
                                                     np.array(list(self.matrix_a_projection_on_orthogonal[:index]) +
                                                              list(self.matrix_a_projection_on_orthogonal[
                                                                   index + 1:]))).success
            if res_1:
                index_for_extra_a_i_new = np.append(index_for_extra_a_i_new, index)
        '''
        index_for_extra_a_i, = np.where(res[:-1] == 0)
        index_for_extra_a_i = np.random.choice(index_for_extra_a_i, size=1)
        # print(f"Проекция a_0 {self.a_0_projection}")
        self.matrix_a_projection_on_orthogonal[index_for_extra_a_i] = self.a_0_projection
        self.matrix_a[index_for_extra_a_i] = self.a_0
        self.points[index_for_extra_a_i] = self.extremal_point
        self.sigma[index_for_extra_a_i] = self.sigma_0
        self.values_of_functions_on_grid[index_for_extra_a_i] = np.array([f(self.extremal_point) for f in self.functions])

        self.index_for_a_0 = index_for_extra_a_i
        self.discarded_point = self.points[index_for_extra_a_i]
        self.discarded_a = self.matrix_a[index_for_extra_a_i]
        self.discarded_a_projection = self.matrix_a_projection_on_orthogonal[index_for_extra_a_i]
        self.discarded_sigma = self.sigma[index_for_extra_a_i]
        self.discarded_values_of_functions_on_grid = self.values_of_functions_on_grid[index_for_extra_a_i]

    def fight_with_degeneracy(self):
        self.degen_or_not_degen = 0
        # print(f"Матрица проекций {self.matrix_a_projection_on_orthogonal}.")
        for num_of_a_j, vertex_a_j in (pair for pair in enumerate(self.matrix_a) if pair[0] != self.index_for_a_0):
            self.values_of_functions_on_grid_without_a_0_and_a_j = \
                np.delete(self.matrix_a, [int(self.index_for_a_0), num_of_a_j], axis=0)
            self.values_of_functions_on_grid_without_a_0_and_a_j_projection = \
                np.delete(self.matrix_a_projection_on_orthogonal, [int(self.index_for_a_0), num_of_a_j], axis=0)
            matrix_and_linear_constraint = \
                np.append(self.values_of_functions_on_grid_without_a_0_and_a_j_projection,
                          [self.linear_functional],
                          axis=0)

            '''
            matrix_and_linear_constraint = \
                np.append(matrix_and_linear_constraint,
                          [np.ones(np.shape(self.values_of_functions_on_grid_without_a_0_and_a_j_projection)[1])],
                          axis=0)
            '''

            # Maybe we will change it later.
            constraint = np.random.rand(np.shape(self.values_of_functions_on_grid_without_a_0_and_a_j_projection)[1])
            constraint /= np.sum(constraint)
            matrix_and_linear_constraint = np.append(matrix_and_linear_constraint, [constraint], axis=0)

            right_part_for_building_polynom = \
                np.zeros(np.shape(self.values_of_functions_on_grid_without_a_0_and_a_j)[0] + 1)
            right_part_with_linear_constraint = np.append(right_part_for_building_polynom, 1)

            self.coefficients_of_polynom_for_fight_with_degeneracy = \
                lstsq(matrix_and_linear_constraint, right_part_with_linear_constraint, rcond=None)[0]

            if not np.allclose(np.dot(matrix_and_linear_constraint,
                                      self.coefficients_of_polynom_for_fight_with_degeneracy),
                               right_part_with_linear_constraint, atol=self.abs_tol_lin_system):
                raise Exception("The solution of linear system is not accurate.")

            def polynom_fight_with_degeneracy(x):
                return np.dot([f(x) for f in self.functions], self.coefficients_of_polynom_for_fight_with_degeneracy)

            if abs(polynom_fight_with_degeneracy(self.extremal_point)) < self.epsilon_degen_polynom:
                self.degen_at_least_once = 1
                self.degen_or_not_degen = 1  # There is a degeneracy at the current step.
                self.num_of_degenerate_vertex = num_of_a_j  # It is a vertex of degenerate simplex.

                left = float(self.extremal_point - self.epsilon_dichotomy)
                right = float(self.extremal_point + self.epsilon_dichotomy)
                if self.upper_bound != np.inf and self.lower_bound != 0:
                    self.extremal_point = dual_annealing(lambda x: -(polynom_fight_with_degeneracy(x) -
                                                                     (self.upper_bound - self.lower_bound) /
                                                                     self.mult_degen) ** 2, [(left, right)]).x
                else:
                    self.extremal_point = dual_annealing(lambda x: (polynom_fight_with_degeneracy(x)) ** 2,
                                                         [(left, right)]).x
                self.sigma_0 = int(np.sign(self.polynom(self.extremal_point)))
                self.a_0 = self.sigma_0 * np.array([f(self.extremal_point) for f in self.functions], dtype=float)
                self.a_0_projection = \
                    self.projection_vector_onto_subspace_orthogonal_to_given_vector(
                        self.a_0, self.linear_functional)
                self.points[self.index_for_a_0] = self.discarded_point
                self.matrix_a[self.index_for_a_0] = self.discarded_a
                self.matrix_a_projection_on_orthogonal[self.index_for_a_0] = self.discarded_a_projection
                self.sigma[self.index_for_a_0] = self.discarded_sigma
                self.values_of_functions_on_grid[self.index_for_a_0] = self.discarded_values_of_functions_on_grid
            break

    def print_in_console(self):
        print(f'Lower bound: {self.lower_bound}; \n Upper bound: {self.upper_bound};\n'
              f'Coefficients of the best polynom: {self.coefficients_of_the_best_polynom};\n'
              f'Number of iterations: {self.number_of_iterations}.\n\n')

    def main_body(self):
        # Algorithm initialization
        self.number_of_iterations = 0
        self.lower_bound = 0
        self.upper_bound = np.inf

        # self.points = np.linspace(self.left, self.right, len(self.functions))
        self.points = np.linspace(self.left + 0.01, self.right, len(self.functions))  # We are a little off zero.
        self.values_of_functions_on_grid = np.array([[f(point) for f in self.functions]
                                                     for point in self.points])  # Rows are the values for fixed point.
        self.projection_values_of_function_on_greed = \
            np.array([self.projection_vector_onto_subspace_orthogonal_to_given_vector(
                v_i, self.linear_functional) for v_i in self.values_of_functions_on_grid])
        eig_values, eig_vectors = eig(self.projection_values_of_function_on_greed.T)
        dict_with_indexes_of_real_eigenvalues = {index: abs(eigenvalue) for index, eigenvalue in enumerate(eig_values)
                                                 if eigenvalue.imag == 0}
        index_for_eigenvalue_zero = min(dict_with_indexes_of_real_eigenvalues.items(), key=lambda x: x[1])[0]
        solution = eig_vectors[:, index_for_eigenvalue_zero].real
        self.sigma = np.sign(solution)
        if not np.all(self.sigma):
            raise Exception("Some components of the vector sigma are equal to zero.")
        self.matrix_a = np.array([sigma_i * values_of_functions_on_grid
                                  for sigma_i, values_of_functions_on_grid
                                  in zip(self.sigma, self.values_of_functions_on_grid)])
        self.matrix_a_projection_on_orthogonal = np.array([sigma_i * projection_values_of_function_on_greed
                                                           for sigma_i, projection_values_of_function_on_greed
                                                           in
                                                           zip(self.sigma,
                                                               self.projection_values_of_function_on_greed)])

        # Iterations of the algorithm.
        while self.upper_bound - self.lower_bound > self.epsilon_lower_upper:
            self.number_of_iterations += 1
            p_and_d_sol = self.solve_linear_system()
            self.coefficients_of_the_best_polynom = p_and_d_sol[:-1]
            self.lower_bound = max(p_and_d_sol[-1], self.lower_bound)
            assert self.lower_bound >= 0, "The lower bound is less than zero."
            self.the_worst_point_for_the_best_approximation()
            self.zero_in_convex_hull()

            # Fight with degeneracy
            self.fight_with_degeneracy()
            if self.degen_or_not_degen == 1:
                self.zero_in_convex_hull()

            self.upper_bound = min(self.extremal_value, self.upper_bound)
            assert self.upper_bound >= 0, "The upper bound is less than zero."

            self.upper_bound_minus_lower_bound = np.append(self.upper_bound_minus_lower_bound,
                                                           self.upper_bound - self.lower_bound)

        self.print_in_console()

        # Let us find the roots of Chebyshev polynomial on the given segment.
        shift = (self.right - self.left) / 20  # self.left + n * shift is an initial guess.
        for n in range(20):
            # new_root = root(self.polynom, np.array(random.uniform(self.left, self.right)))
            new_root = root(self.polynom, np.array(self.left + n * shift))
            if new_root.success and not np.any(np.isclose(self.roots, new_root.x, self.epsilon_root)) \
                    and self.left <= new_root.x <= self.right:
                self.roots = np.append(self.roots, new_root.x)
        self.roots = np.unique(self.roots)
        print(f"Roots of the polynomial of the best approximation {self.roots}.")

        # Let us write the results into the file.
        Path(Path.cwd().parent.parent / "results" / "chebyshev_polynomials" /
             self.dir_with_results).mkdir(parents=True, exist_ok=True)
        results = Path.cwd().parent.parent / "results" / "chebyshev_polynomials" / \
                  self.dir_with_results / self.file_with_results
        target = results.open("w", encoding="UTF-8")
        dict_with_results = {'coefficients_of_the_best_polynom': list(self.coefficients_of_the_best_polynom),
                             'alternance_points': list(self.points),
                             'roots_of_the_best_polynom': list(self.roots),
                             'upper_bound_minus_lower_bound_at_each_step': list(self.upper_bound_minus_lower_bound)}
        json.dump(dict_with_results, target, indent=4)

        # Figure with the "Chebyshev polynomial" w.r.t the given system.
        temp_address = results.stem + '_polynomial'
        fig = plt.figure()
        a = fig.add_axes([0, 0, 1, 1])
        a.set_title(r'Chebyshev Polynomial $p$')
        a.set_xlabel('Points')
        a.set_ylabel(r'$p$')
        a.grid(color='b', ls='-.', lw=0.5)
        points = np.arange(self.left, self.right + 0.01, 0.01)
        values_of_polynomial = np.array([lambda x=y: self.polynom(x) for y in points])
        a.plot(points, [value() for value in values_of_polynomial], 'b', label=r'$p$')
        a.set_xticks(np.arange(self.left, self.right + 0.01, (self.right - self.left) / 10))
        a.legend(loc='upper right')
        fig.savefig(results.parent / temp_address, bbox_inches='tight')

        # Figure with the alternance for final polynomial of best approximation.
        temp_address = results.stem + '_alternance'
        fig = plt.figure()
        a = fig.add_axes([0, 0, 1, 1])
        a.set_title(r'Chebyshev Polynomial $p$')
        a.set_xlabel('Points')
        a.set_ylabel(r'$p$')
        a.grid(color='b', ls='-.', lw=0.5)
        points = np.arange(self.left, self.right + 0.01, 0.01)
        values = np.array([lambda x=y: self.polynom(x) for y in points])
        a.plot(points, [value() for value in values], 'r')
        values_in_alternance_points = np.array([lambda x=y: self.polynom(x) for y in self.points])
        a.scatter(self.points, [value() for value in values_in_alternance_points], color='black')
        a.scatter(self.roots, np.zeros(len(self.roots)), color='black')
        a.set_xticks(np.arange(self.left, self.right + 0.01, (self.right - self.left) / 10))
        a.vlines(x=self.points, ymin=0, ymax=[value() for value in values_in_alternance_points],
                 color='green',
                 ls='--')
        a.hlines(y=0, xmin=self.left, xmax=self.right, color='blue')
        fig.savefig(results.parent / temp_address, bbox_inches='tight')
        # Figure with the rate of convergence.
        temp_address = results.stem + '_convergence'
        upper_minus_lower = dict_with_results['upper_bound_minus_lower_bound_at_each_step']

        fig = plt.figure()
        a = fig.add_axes([0, 0, 1, 1])

        a.set_title("Convergence Rate")
        a.set_xlabel("Iteration number")
        a.set_ylabel(r"$ln(ub-lb)$")
        a.grid(color='b', ls='-.', lw=0.5)
        a.plot(np.arange(1, len(np.log([ub_m_lb for ub_m_lb in upper_minus_lower if
                                        ub_m_lb > 0])) + 1), np.log([ub_m_lb for ub_m_lb in upper_minus_lower if
                                                                     ub_m_lb > 0]), 'ro-')
        a.set_xticks(np.arange(1, len(np.log([ub_m_lb for ub_m_lb in upper_minus_lower if
                                              ub_m_lb > 0])) + 1, 2))
        fig.savefig(results.parent / temp_address, bbox_inches='tight')

        return results
