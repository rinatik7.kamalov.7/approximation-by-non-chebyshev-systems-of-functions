import json
import math as m
import numpy as np
from numpy.linalg import lstsq, matrix_rank
from pathlib import Path
from scipy.optimize import dual_annealing


class InterpolationChebyshevSystem:
    def __init__(self, f_0, functions, points_of_interpolation,
                 left=0, right=1, abs_tol_lin_system=1e-6):
        # self.parent_folder = parent_folder
        # self.dir_with_results = file_with_results
        # self.file_with_results = file_with_results + '.json'

        self.f_0 = f_0
        self.left = left
        self.right = right

        self.functions = functions
        self.abs_tol_lin_system = abs_tol_lin_system
        self.points_of_interpolation = points_of_interpolation
        self.values_of_functions_on_grid = np.array([[f(point) for f in self.functions]
                                                     for point in self.points_of_interpolation])
        self.values_of_f_0_on_grid = np.array([f_0(point) for point in self.points_of_interpolation])
        self.coefficients_of_the_best_polynom = None

    def interpolation(self):
        """We solve the linear system Ax=b by using lstsq method."""
        coefficients = lstsq(self.values_of_functions_on_grid, self.values_of_f_0_on_grid, rcond=None)[0]
        return coefficients

    def error_of_interpolation(self):
        """We find the error of interpolation."""
        def polynom(x):
            return np.dot([f(x) for f in self.functions], self.coefficients_of_the_best_polynom)

        extremal_point, extremal_value = (dual_annealing(lambda x: -(polynom(x) - self.f_0(x)) ** 2,
                                                         [(self.left, self.right)]).x,
                                          m.sqrt(- dual_annealing(lambda x: -(polynom(x) - self.f_0(x)) ** 2,
                                                                  [(self.left, self.right)]).fun))
        return extremal_point, extremal_value

    def main_body(self):
        self.coefficients_of_the_best_polynom = self.interpolation()
        extremal_value = self.error_of_interpolation()[1]

        return extremal_value





