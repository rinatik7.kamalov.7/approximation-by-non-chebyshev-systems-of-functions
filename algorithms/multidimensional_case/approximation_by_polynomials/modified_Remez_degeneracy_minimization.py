import json
import math as m
import matplotlib.pyplot as plt
import numpy as np
import warnings
from mpl_toolkits import mplot3d
from numpy.linalg import lstsq, matrix_rank
from pathlib import Path
from scipy.optimize import dual_annealing, linprog


class ModifiedRemezAlgorithmFightWithDegeneracyMinimization:
    def __init__(self, f_0, functions, dimension, parent_folder, file_with_results=None, initial_points=None, left=0,
                 right=1, abs_tol_lin_system=1e-6, epsilon_lower_upper=1e-6, mult_degen=10,
                 epsilon_degen_polynom=1e-5, epsilon_dichotomy=1e-7):
        warnings.filterwarnings("ignore", category=np.VisibleDeprecationWarning)
        self.parent_folder = parent_folder
        self.dir_with_results = file_with_results
        if file_with_results:
            self.file_with_results = file_with_results + '.json'

        self.f_0 = f_0
        self.dimension = dimension  # Number of variables of f_0.
        self.left = left
        self.right = right

        '''
        # Uniform grid on R^d.
        points = np.stack(np.meshgrid(*(self.dimension * [list(np.linspace(left, right, len(functions) + 1))])),
                          axis=-1)
        self.points = np.reshape(points, (int(points.size / self.dimension), self.dimension)) \
            if not initial_points else np.array(initial_points)  # Points on our multidimensional grid.
        '''

        self.points = self.left + np.random.rand(len(functions) + 1, self.dimension) * \
                      (self.right - self.left)  # Points on our multidimensional grid.
        # print(f"Points {self.points}")
        self.functions = functions
        self.abs_tol_lin_system = abs_tol_lin_system
        self.epsilon_lower_upper = epsilon_lower_upper
        self.mult_degen = mult_degen
        self.epsilon_degen_polynom = epsilon_degen_polynom
        self.epsilon_dichotomy = epsilon_dichotomy

        self.number_of_iterations = 0
        self.lower_bound = 0
        self.upper_bound = np.inf
        self.coefficients_of_the_best_polynom = None
        self.upper_bound_minus_lower_bound = np.array([])
        self.values_of_functions_on_grid = np.array([[f(point) for f in self.functions]
                                                     for point in self.points])  # Rows are values for fixed point.
        # print(f"Values of functions on grid {self.values_of_functions_on_grid}.")
        self.matrix_a = None  # Rows of the matrix are vectors a_i.
        self.sigma = None
        self.values_of_f_0_on_grid = np.array([f_0(point) for point in self.points])
        # print(f"Values of f_0 on grid {self.values_of_f_0_on_grid}.")
        self.extremal_point = None
        self.extremal_value = None
        self.a_0 = None
        self.index_for_a_0 = None
        self.sigma_0 = None

        self.coefficients_of_polynom_for_fight_with_degeneracy = None
        self.values_of_functions_on_grid_without_a_0_and_a_j = None
        self.matrix_with_a_0_instead_of_a_i = None
        self.points_without_added_and_discarded_points = None

        self.discarded_point = None
        self.discarded_a = None
        self.discarded_sigma = None
        self.discarded_values_of_functions_on_grid = None
        self.discarded_values_of_f_0_on_grid = None

        self.num_of_degenerate_vertex = -1
        self.degen_or_not_degen = 0
        self.degen_at_least_once = 0

    def solve_linear_system(self):
        matrix_lin_system = np.c_[self.matrix_a, np.full(np.shape(self.matrix_a)[0], -1)]
        assert np.shape(self.sigma) == np.shape(self.values_of_f_0_on_grid), "Shapes of sigma and values of f_0 on " \
                                                                             "the grid do not match."
        vector_b = np.multiply(self.sigma, self.values_of_f_0_on_grid)
        p_and_d_sol = lstsq(matrix_lin_system, vector_b, rcond=None)[0]
        if not np.allclose(np.dot(matrix_lin_system, p_and_d_sol), vector_b, atol=self.abs_tol_lin_system):
            raise Exception("The solution of the linear system for lower bound is not accurate.")
        return p_and_d_sol

    def polynom(self, x):
        return np.dot([f(x) for f in self.functions], self.coefficients_of_the_best_polynom)

    def the_worst_point_for_the_best_approximation(self):
        self.extremal_point, self.extremal_value = (dual_annealing(lambda x: -(self.polynom(x) - self.f_0(x)) ** 2,
                                                                   self.dimension * [(self.left, self.right)]).x,
                                                    m.sqrt(
                                                        - dual_annealing(lambda x: -(self.polynom(x)
                                                                                     - self.f_0(x)) ** 2,
                                                                         self.dimension *
                                                                         [(self.left, self.right)]).fun))
        self.sigma_0 = int(np.sign(self.polynom(self.extremal_point) - self.f_0(self.extremal_point)))
        self.a_0 = self.sigma_0 * np.array([f(self.extremal_point) for f in self.functions], dtype=float)

    def minus_intersection_ray_with_face(self, vector, vectors):
        matrix_equalities = np.c_[vectors.T, vector]
        last_raw_in_matrix = np.array([np.append(np.ones(np.shape(matrix_equalities)[1] - 1), 0)])
        matrix_equalities = np.append(matrix_equalities, last_raw_in_matrix, axis=0)
        vector_equalities = np.append(np.zeros(np.shape(matrix_equalities)[0] - 1), 1)
        objective_vector = np.append(np.zeros(np.shape(matrix_equalities)[1] - 1), -1)
        res = linprog(objective_vector, A_eq=matrix_equalities, b_eq=vector_equalities, method='highs-ds')
        if not res.success:
            raise Exception(f"The vector a_0 does not lie in the convex hull of previous vectors. The matrix a is \n "
                            f"{self.matrix_a}. The vector a_0 is {self.a_0}.")
        return res.x

    def zero_in_convex_hull(self):
        res = self.minus_intersection_ray_with_face(self.a_0, self.matrix_a)
        index_for_extra_a_i, = np.where(res[:-1] == 0)
        index_for_extra_a_i = np.random.choice(index_for_extra_a_i, size=1)

        # Here we write the discarded vector for fight with degeneracy.
        self.matrix_a[index_for_extra_a_i] = self.a_0
        self.index_for_a_0 = index_for_extra_a_i
        self.discarded_point = self.points[index_for_extra_a_i]
        self.discarded_a = self.matrix_a[index_for_extra_a_i]
        self.discarded_sigma = self.sigma[index_for_extra_a_i]
        self.discarded_values_of_functions_on_grid = self.values_of_functions_on_grid[index_for_extra_a_i]
        self.discarded_values_of_f_0_on_grid = self.values_of_f_0_on_grid[index_for_extra_a_i]

        self.points[index_for_extra_a_i] = self.extremal_point
        self.matrix_a[index_for_extra_a_i] = self.a_0
        self.sigma[index_for_extra_a_i] = self.sigma_0
        self.values_of_functions_on_grid[index_for_extra_a_i] = np.array([f(self.extremal_point) for f in self.functions])
        self.values_of_f_0_on_grid[index_for_extra_a_i] = self.f_0(self.extremal_point)

    def fight_with_degeneracy(self):
        self.degen_or_not_degen = 0
        for num_of_a_j, vertex_a_j in (pair for pair in enumerate(self.matrix_a) if pair[0] != self.index_for_a_0):
            self.values_of_functions_on_grid_without_a_0_and_a_j = \
                np.delete(self.matrix_a, [int(self.index_for_a_0), num_of_a_j], axis=0)

            '''
            matrix_and_linear_constraint = \
                np.append(self.values_of_functions_on_grid_without_a_0_and_a_j,
                          [np.ones(np.shape(self.values_of_functions_on_grid_without_a_0_and_a_j)[1])],
                          axis=0)
            '''

            # Maybe we will change it later.
            constraint = np.random.rand(np.shape(self.values_of_functions_on_grid_without_a_0_and_a_j)[1])
            constraint /= np.sum(constraint)
            matrix_and_linear_constraint = np.append(self.values_of_functions_on_grid_without_a_0_and_a_j,
                                                     [constraint],
                                                     axis=0)

            right_part_for_building_polynom = \
                np.zeros(np.shape(self.values_of_functions_on_grid_without_a_0_and_a_j)[0])
            right_part_with_linear_constraint = np.append(right_part_for_building_polynom, 1)
            self.coefficients_of_polynom_for_fight_with_degeneracy = \
                lstsq(matrix_and_linear_constraint, right_part_with_linear_constraint, rcond=None)[0]
            if not np.allclose(np.dot(matrix_and_linear_constraint,
                                      self.coefficients_of_polynom_for_fight_with_degeneracy),
                               right_part_with_linear_constraint, atol=self.abs_tol_lin_system):
                raise Exception("The solution of the linear system for fight with degeneracy is not accurate.")

            def polynom_fight_with_degeneracy(x):
                return np.dot([f(x) for f in self.functions], self.coefficients_of_polynom_for_fight_with_degeneracy)

            if abs(polynom_fight_with_degeneracy(self.extremal_point)) < self.epsilon_degen_polynom:
                self.degen_at_least_once = 1
                self.degen_or_not_degen = 1  # There is a degeneracy on the current step.
                self.num_of_degenerate_vertex = num_of_a_j  # It is a vertex of the degenerate simplex.

                if self.upper_bound != np.inf and self.lower_bound != 0:
                    left = float(self.extremal_point - self.epsilon_dichotomy)
                    right = float(self.extremal_point + self.epsilon_dichotomy)
                    self.extremal_point = dual_annealing(lambda x: -(polynom_fight_with_degeneracy(x) -
                                                                     (self.upper_bound - self.lower_bound) /
                                                                     self.mult_degen) ** 2, [(left, right)]).x
                else:
                    self.extremal_point = self.extremal_point - self.epsilon_dichotomy
                self.sigma_0 = int(np.sign(self.polynom(self.extremal_point) - self.f_0(self.extremal_point)))
                self.a_0 = self.sigma_0 * np.array([f(self.extremal_point) for f in self.functions], dtype=float)
                self.points[self.index_for_a_0] = self.discarded_point
                self.matrix_a[self.index_for_a_0] = self.discarded_a
                self.sigma[self.index_for_a_0] = self.discarded_sigma
                self.values_of_functions_on_grid[self.index_for_a_0] = self.discarded_values_of_functions_on_grid
                self.values_of_f_0_on_grid[self.index_for_a_0] = self.discarded_values_of_f_0_on_grid

            break

    def print_in_console(self):
        print(f'Lower bound: {self.lower_bound}; \n Upper bound: {self.upper_bound};\n'
              f'Coefficients of the best polynom: {self.coefficients_of_the_best_polynom};\n'
              f'Alternance points: {self.points};\n\n')

    def main_body(self):
        # Algorithm initialization.
        if matrix_rank(self.values_of_functions_on_grid) != \
                np.shape(self.values_of_functions_on_grid)[1]:
            raise Exception("The original matrix has an incomplete rank.")
        values_of_func_on_grid_and_linear_constraint = \
            np.append(self.values_of_functions_on_grid.T,
                      [np.ones(np.shape(self.values_of_functions_on_grid)[0])], axis=0)
        right_part_without_linear_constraint = [0] * (np.shape(self.values_of_functions_on_grid)[1])
        right_part_with_linear_constraint = np.append(right_part_without_linear_constraint, 1)
        solution = lstsq(values_of_func_on_grid_and_linear_constraint, right_part_with_linear_constraint, rcond=None)[0]
        if not np.allclose(np.dot(values_of_func_on_grid_and_linear_constraint, solution),
                           right_part_with_linear_constraint, atol=self.abs_tol_lin_system):
            raise Exception("The solution of linear system for initialization is not accurate.")
        self.sigma = np.sign(solution)
        if not np.all(self.sigma):
            raise Exception("Some components of vector sigma are equal to zero.")
        self.matrix_a = np.array([sigma_i * values_of_functions_on_grid_column
                                  for sigma_i, values_of_functions_on_grid_column
                                  in zip(self.sigma, self.values_of_functions_on_grid)])

        # Iterations of the algorithm.
        while self.upper_bound - self.lower_bound > self.epsilon_lower_upper:
            self.number_of_iterations += 1
            p_and_d_sol = self.solve_linear_system()
            self.coefficients_of_the_best_polynom = p_and_d_sol[:-1]
            self.lower_bound = max(p_and_d_sol[-1], self.lower_bound)
            assert self.lower_bound >= 0, "The lower bound is less than zero."
            self.the_worst_point_for_the_best_approximation()
            self.zero_in_convex_hull()

            # Fight with degeneracy
            self.fight_with_degeneracy()
            if self.degen_or_not_degen == 1:
                self.zero_in_convex_hull()

            self.upper_bound = min(self.extremal_value, self.upper_bound)
            assert self.upper_bound >= 0, "The upper bound is less than zero."

            if self.upper_bound - self.lower_bound >= 0:
                self.upper_bound_minus_lower_bound = np.append(self.upper_bound_minus_lower_bound,
                                                               self.upper_bound - self.lower_bound)

            if self.parent_folder == "modified_Remez_algorithm" or \
                    self.parent_folder == "modified_Remez_fight_minimization":
                self.print_in_console()

        if self.parent_folder == "modified_Remez_algorithm" or \
                self.parent_folder == "modified_Remez_fight_minimization":

            # Let us write the results into the file.
            Path(Path.cwd().parent.parent / "results" / "modified_Remez_fight_minimization" /
                 self.dir_with_results).mkdir(parents=True, exist_ok=True)
            results = Path.cwd().parent.parent / "results" / "modified_Remez_fight_minimization" / \
                      self.dir_with_results / self.file_with_results
            target = results.open("w", encoding="UTF-8")
            dict_with_results = {'coefficients_of_the_best_polynom': list(self.coefficients_of_the_best_polynom),
                                 'alternance_points': list([list(point) for point in self.points]),
                                 'upper_bound_minus_lower_bound_at_each_step': list(self.upper_bound_minus_lower_bound)}
            # print(f"Dictionary with results {dict_with_results}.")
            json.dump(dict_with_results, target, indent=4)

            # Figure with the rate of convergence.
            temp_address = results.stem + '_convergence'
            upper_minus_lower = dict_with_results['upper_bound_minus_lower_bound_at_each_step']

            fig = plt.figure()
            a = fig.add_axes([0, 0, 1, 1])

            a.set_title("Convergence Rate")
            a.set_xlabel("Iteration Number")
            a.set_ylabel(r"$ln(ub-lb)$")
            a.grid(color='b', ls='-.', lw=0.5)
            a.plot(np.arange(1, len(np.log(upper_minus_lower)) + 1), np.log(upper_minus_lower), 'ro-')
            a.set_xticks(np.arange(1, len(np.log(upper_minus_lower)) + 1, 2))

            fig.savefig(results.parent / temp_address, bbox_inches='tight')

            '''
            # We draw pictures in 2D.
            if self.dimension == 2:
                # Figure with the function f_0 and polynomial of the best approximation.
                temp_address = results.stem + '_function_and_polynomial'
                fig = plt.figure()
                a = fig.add_subplot(111, projection='3d')
                a.set_title(r'Objective Function $f_0$ and Polynomial of Best Approximation $p$')
                a.set_xlabel(r'$x$')
                a.set_ylabel(r'$y$')
                a.grid(color='b', ls='-.', lw=0.5)
                grid_2_d = np.stack(np.meshgrid(*(self.dimension *
                                                  [list(np.arange(self.left, self.right + 0.01,
                                                                  0.01))])),
                                    axis=-1)
                grid_2_d = np.reshape(grid_2_d, (int(grid_2_d.size / self.dimension), self.dimension))
                values_of_f_0 = np.array([lambda x=y: self.f_0(x) for y in grid_2_d])
                values_of_polynomial = np.array([lambda x=y: self.polynom(x) for y in grid_2_d])
                X, Y = np.meshgrid(np.arange(self.left, self.right + 0.01, 0.01),
                                   np.arange(self.left, self.right + 0.01, 0.01))
                coordinates = list(zip(X.ravel(), Y.ravel()))
                values_of_f_0 = np.array([lambda x=y: self.f_0(x) for y in coordinates])
                # print(f"Значения f_0 {[value() for value in values_of_f_0]}")
                print(f"Сетка {X, Y}")
                print(f"Тестовая функция {np.sin(X) + np.cos(Y)}")
                a.plot_surface(X, Y, np.array([value() for value in values_of_f_0]),
                               cmap='viridis')
                a.plot_surface(np.arange(self.left, self.right + 0.01, 0.01),
                               np.arange(self.left, self.right + 0.01, 0.01),
                               [value() for value in values_of_polynomial], cmap='viridis')
                a.set_xticks(np.arange(self.left, self.right + 0.01, (self.right - self.left) / 10))
                a.legend(loc='upper right')
        
                fig.savefig(results.parent / temp_address, bbox_inches='tight')

                # Figure with the alternance for final polynomial of best approximation.
                temp_address = results.stem + '_alternance'
                fig = plt.figure()
                a = fig.add_axes([0, 0, 1, 1])
                a.set_title(r'Difference of Objective Function $f_0$ and Polynomial $p$')
                a.set_xlabel('Points')
                a.set_ylabel(r'$f_0-p$')
                a.grid(color='b', ls='-.', lw=0.5)
                points = np.arange(self.left, self.right + 0.01, 0.01)
                values = np.array([lambda x=y: self.f_0(x) - self.polynom(x) for y in points])
                a.plot(points, [value() for value in values], 'r')
                values_in_alternance_points = np.array([lambda x=y: self.f_0(x) - self.polynom(x) for y in self.points])
                a.scatter(self.points, [value() for value in values_in_alternance_points], color='black')
                a.set_xticks(np.arange(self.left, self.right + 0.01, (self.right - self.left) / 10))
                a.vlines(x=self.points, ymin=0, ymax=[value() for value in values_in_alternance_points],
                         color='green',
                         ls='--')
                a.hlines(y=0, xmin=self.left, xmax=self.right, color='blue')
                fig.savefig(results.parent / temp_address, bbox_inches='tight')
                '''
            return results
